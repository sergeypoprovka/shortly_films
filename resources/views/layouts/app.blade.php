<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="theme-color" content="#F29559">
  <meta name="msapplication-config" content="/img/icons/browserconfig.xml">



  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>
  <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>

  <script src="https://vjs.zencdn.net/5.19/video.min.js"></script>
  <script src="/js/videojs-resolution-switcher/lib/videojs-resolution-switcher.js"></script>
  <!-- Styles -->
  <link rel="stylesheet" href="/css/video-js.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="{{ asset_path('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset_path('css/responsive.css') }}" rel="stylesheet">
  <link rel="apple-touch-icon" sizes="180x180" href="/img/icons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/icons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/icons/favicon-16x16.png">
  <link rel="mask-icon" href="/img/icons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="/img/icons/favicon.ico">

  <link rel="manifest" href="/manifest.json">

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-97855882-1"></script>
  <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-97855882-1');
  </script>

</head>

<body>
  <div id="app">
    @include('elements.header')
    <div class="container-fluid h100">
      @if (auth()->check())
      <div class="subscribers-sidebar" @guest style="background:#283845" @endguest>
        <span class="hidden-sm hidden-md hidden-lg">
          <feedback></feedback>
        </span>
        @auth
          @include('elements.user-sidebar')
          <div class="user-follows">
            <sidebar-follows></sidebar-follows>
          </div>
        @endauth
      </div>
      @endif
      <div class="content-full @if(auth()->check()) {{ 'has-sidebar' }} @endif">
        @include('elements.flash')
        @yield('content')
        @include('elements.bottomnav')
        @if(!auth()->check() || auth()->check() && auth()->user()->role == 'Filmmaker')
          @include('elements.registration-modal')
        @endif
      </div>
      <!-- .content-full -->
    </div>
    <!-- .container-fluid .h100 -->
    <span class="hidden-xs">
      <feedback></feedback>
    </span>
  </div>
  <!-- #app -->

  <!-- Scripts -->
  <script src="https://js.stripe.com/v3/"></script>
  <script src="{{ asset_path('js/app.js') }}"></script>
  <script>
      window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>
  </script>
  @yield('js')
</body>

</html>