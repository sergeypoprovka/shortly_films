<!DOCTYPE html>
<html lang="en">
    <head>        
        @include('elements.headerfiles')
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-97855882-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-97855882-1');
        </script>
    </head>
    <body>
    <div id="app">

                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                        <li class="logo text-center">
                            <a href="/"><img height="43px" src="/img/logo.png"> </a>
                        </li>

                    <!-- TOGGLE NAVIGATION -->
                    {{--<li class="xn-icon-button">--}}
                        {{--<a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>--}}
                    {{--</li>--}}
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    {{--<li class="xn-search">--}}
                        <!-- <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form> -->
                    {{--</li>   --}}
                    <!-- END SEARCH -->
                    <!-- POWER OFF -->
                    <li class="xn-icon-button pull-right last">
                        <a href="#"><span class="fa fa-power-off"></span></a>
                        <ul class="xn-drop-left animated zoomIn">
<!--                             <li><a href="pages-lock-screen.html"><span class="fa fa-lock"></span> Lock Screen</a></li> -->
                            <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>
                        </ul>                        
                    </li> 
                    <!-- END POWER OFF -->
                    <!-- MESSAGES -->

                </ul>
                <div class="clearfix"></div>
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                @if (count($errors) > 0)
                <br/>
                <div class="alert alert-warning" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-success" role="success">
                    <ul>
                        <li>{{ Session::get('success') }}</li>
                    </ul>
                </div>
                @endif

                @yield('content')
                

                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->
        
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <form method="post" action="/logout">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-success btn-lg">{{__('Yes')}}</button>
                                <button class="btn btn-default btn-lg mb-control-close">{{__('No')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript">
            var APP_URL = {!! json_encode(url('/')) !!}
            // var trkt = {!! env('TRK') !!}
        </script>
        @include('elements.footerfiles');
    </body>
</html>






