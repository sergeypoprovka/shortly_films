<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="https://vjs.zencdn.net/5.19/video.min.js"></script>
    <script src="/js/videojs-resolution-switcher/lib/videojs-resolution-switcher.js"></script>
    <!-- Styles -->
    <link href="{{ asset('css/theme-atlant-default.css') }}" rel="stylesheet">
    <link href="{{ asset_path('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/css/video-js.css" />
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-97855882-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-97855882-1');
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top navbar-inverse">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="/img/logo.png" height="24px" />
                    </a>
                </div>
                <form method="get" action="/search" class="navbar-form navbar-left search-form">
                    {{ csrf_field() }}
                    <div class="form-group search-wrap">
                        <input type="text" class="form-control" name="q" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-primary submit-search">Submit</button>
                </form>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">{{__('Login')}}</a></li>
                            <li><a href="{{ route('registertype') }}">{{__('Register')}}</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->profile->name }} {{ Auth::user()->profile->lastname }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    @if(Auth::user()->role == "Administrator") <li> <a href="/admin"><i class="fa fa-key"></i> {{__('Administrator')}}</a></li> @endif
                                    <li> <a href="/user/subscriptions"><i class="fa fa-cubes"></i> {{__('Subscriptions')}}</a></li>
                                    <li> <a href="/user/profile"><i class="fa fa-user"></i> {{__('Profile')}}</a></li>
                                    <li> <a href="/user/changepassword"><i class="fa fa-lock"></i> {{__('Change password')}}</a></li>
                                    <li> <a href="/user/payments"><i class="fa fa-money"></i> {{__('Payments')}}</a></li>
                                    @if(Auth::user()->role == "Administrator" || Auth::user()->role == "Filmmaker")<li> <a href="/user/channel"><i class="fa fa-video-camera"></i> {{__('Channel')}}</a></li>@endif
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out"></i> {{__('Logout')}}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @can('upload', (new \App\Models\Video()))
                                <li>
                                    <a href="/film/upload" class="btn btn-primary">{{__('Upload video')}}</a>
                                </li>
                            @endcan
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @include('elements.flash')
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset_path('js/app.js') }}"></script>
    @yield('js')
</body>
</html>
