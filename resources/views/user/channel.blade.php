@extends('layouts.clear')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="container-fluid">
                            <h2>{{__('My channel')}}</h2>
                            <div class="row">
                                @foreach($videos as $video)
                                    <div class="col-md-3">
                                        <div class="video-wrapper" style="background-image: @if($video->main_thumb_url)
                                                url('{{ $video->main_thumb_url}}');
                                        @else
                                                url( {{ (\Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($video->aws_prefix_folder.'720p_16-9__-00001.png')) ? \Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($video->aws_prefix_folder.'720p_16-9__-00002.png') : ''}}
                                        @endif
                                                ">

                                            <div class="clearfix"></div>
                                            <div class="video-title">
                                                <h4>
                                                    @if(!auth()->check() || auth()->check() && auth()->user()->role == 'Filmmaker' && $video->user_id != auth()->user()->id)
                                                        <a href="#" data-toggle="modal" data-target="#registerScreen">
                                                    @else
                                                        <a href="/video/{{$video->id}}">
                                                    @endif
                                                        {{ $video->title }}
                                                    </a>
                                                </h4>
                                            </div>
                                            <p>
                                                {{__('Uploaded')}}: {{ $video->created_at }}
                                            </p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="text-center">
                                {{ $videos->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
