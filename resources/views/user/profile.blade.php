@extends('layouts.clear')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="container-fluid">
                            <h2>{{__('My profile')}}</h2>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="text-center profile-image">
                                        <img src="{{ Auth::user()->profile->image->profile->url }}" width="100px" height="100px" class="img-circle" />
                                    </div>
                                    @include('elements.errors')
                                    <br>
                                    <form method="post" action="/user/profile" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="name">{{__('First name')}}</label>
                                                    <input type="text" name="name" class="form-control" id="name" value="{{ Auth::user()->profile->name }}" />
                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="lastname">{{__('Last name')}}</label>
                                                    <input type="text" name="lastname" class="form-control" id="lastname" value="{{ Auth::user()->profile->lastname }}" />
                                                    @if ($errors->has('lastname'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('lastname') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="image">{{__('User image')}}</label>
                                                    <input type="file" name="image" class="form-control" id="image" />
                                                    @if ($errors->has('image'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('image') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="address">{{__('Address')}}</label>
                                                    <textarea name="address" class="form-control" id="address">{{ Auth::user()->profile->address }}</textarea>
                                                    @if ($errors->has('address'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('address') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="email">{{__('E-mail')}}</label>
                                                    <input type="text" name="email" class="form-control" id="email" value="{{ Auth::user()->email }}" />
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="country">{{__('Country')}}</label>
                                                    <input type="text" name="country" class="form-control" id="country" value="{{ Auth::user()->profile->country }}" />
                                                    @if ($errors->has('country'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('country') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="city">{{__('City')}}</label>
                                                    <input type="text" name="city" class="form-control" id="city" value="{{ Auth::user()->profile->city }}" />
                                                    @if ($errors->has('city'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('city') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="gender">{{__('Gender')}}</label>
                                                    <select name="gender" class="form-control">
                                                        <option selected disabled value="">{{__('Select gender')}}</option>
                                                        <option @if(Auth::user()->profile->gender == "male") selected @endif value="male">{{__('Male')}}</option>
                                                        <option @if(Auth::user()->profile->gender == "female") selected @endif value="female">{{__('Female')}}</option>
                                                    </select>
                                                    @if ($errors->has('gender'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('gender') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="zip">{{__('Zip code')}}</label>
                                                    <input type="text" name="zip" class="form-control" id="zip" value="{{ Auth::user()->profile->zip }}" />
                                                    @if ($errors->has('zip'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('zip') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="company">{{__('Company')}}</label>
                                                    <input type="text" name="company" class="form-control" id="company" value="{{ Auth::user()->profile->company }}" />
                                                    @if ($errors->has('company'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('company') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="iban">{{__('IBAN')}}</label>
                                                    <input type="text" name="iban" class="form-control" id="iban" value="{{ Auth::user()->profile->iban }}" />
                                                    @if ($errors->has('iban'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('iban') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="corporate_number">{{__('Corporate identity number')}}</label>
                                                    <input type="text" name="corporate_number" class="form-control" id="corporate_number" value="{{ Auth::user()->profile->corporate_number }}" />
                                                    @if ($errors->has('corporate_number'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('corporate_number') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="description">{{__('Short description')}}</label>
                                                    <textarea name="description" class="form-control" id="description">{{ Auth::user()->profile->description }}</textarea>
                                                    @if ($errors->has('description'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('description') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br>
                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-primary btn-lg">{{__('Save profile')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection