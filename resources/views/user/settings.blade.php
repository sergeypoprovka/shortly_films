@extends('layouts.app')

@section('content')
    <div class="container container--user-settings">
        <div class="row">
            <div class="col-md-3">
                <setting-groups :role="'{{ auth()->user()->role }}'" :group="'membership'"></setting-groups>
            </div>
            <div class="col-md-9">
                <setting-blocks :csrf="'{{ csrf_token() }}'" :user="{{ $user_account }}"></setting-blocks>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=owyhqfmyc4ozprwugxlp7ubrfi0kp72n285obhnabnwjxolq"></script>
    <script>

    </script>
@endsection