@extends('layouts.clear')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="container-fluid">
                            <h2>{{__('Change password')}}</h2>
                            @include('elements.errors')
                            <form method="post" action="changepassword">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="control-label" for="password">{{__('New password')}}</label>
                                    <input type="password" name="password" id="password" class="form-control" />
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label class="control-label" for="password_confirmation">{{__('Password confirmation')}}</label>
                                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" />
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg">{{__('Update password')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection