@extends('layouts.app')

@section('content')
<div class="container">
    {{--<h3 class="static-header">{{__('Watch them again')}}</h3>--}}
    {{--<hr>--}}
    @if(count($history) > 0)
    @foreach($history as $item)
            <div class="row history-item">
                <div class="col-md-3">
                    <div class="video-wrapper">
                        @if(!auth()->check() || auth()->check() && auth()->user()->role == 'Filmmaker' && $item->video->user_id != auth()->user()->id)
                            <a href="#" data-toggle="modal" data-target="#registerScreen">
                        @else
                            <a href="/video/{{$item->video->id}}">
                        @endif
                            <div class="video-image" style="background-image: @if($item->video->main_thumb_url)
                                    url('{{ $item->video->main_thumb_url}}');
                            @else
                                    url( {{ (\Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($item->video->aws_prefix_folder.'720p_16-9__-00001.png')) ? \Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($item->video->aws_prefix_folder.'720p_16-9__-00002.png') : ''}}
                            @endif
                                    ">
                                <div class="timer">
                                    <img src="/img/timer_icon.svg" /> {{ date('i:s',strtotime($item->video->videoDuration)) }}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="video-title">
                        <h4>
                            @if(!auth()->check() || auth()->check() && auth()->user()->role == 'Filmmaker' && $item->video->user_id != auth()->user()->id)
                                <a href="#" data-toggle="modal" data-target="#registerScreen">
                            @else
                                <a href="/video/{{$item->video->id}}">
                            @endif
                                {{ $item->video->title }}
                            </a>
                        </h4>
                    </div>
                        <p class="video-author">
                            @if(isset($item->video->user->profile->image))
                                <img src="{{ $item->video->user->profile->image->profile->url }}" class="avatar-small" />
                            @else
                                <img src="https://placehold.it/48x48" class="avatar-small" />
                            @endif
                            <a href="/author/{{ $item->video->user_id }}"> {{ $item->video->user->profile->name }} {{ $item->video->user->profile->lastname }}</a>
                        </p>
                </div>
            </div>
    @endforeach
    <div class="text-center">
        {{ $history->links() }}
    </div>
    @else
        <p>Start writing your own history.</p>
    @endif
</div>
@endsection