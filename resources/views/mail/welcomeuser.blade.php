@extends('layouts.mail')

@section('content')
    <h1>Hi, {{ $user->profile->name." ".$user->profile->lastname }}</h1>
    <p>Thanks for joining the short-film revolution!</p>
    <p>At Shortly, we're determined to return short films to their former glory and make them easily available to anybody in the world. And we're serious about ensuring makers of short films are properly compensated for their art.</p>
    <p>We’re working frantically to build up our database so that you can watch all the short films you fancy.</p>
    <p>If you are a filmmaker, you can start uploading your films today!</p>
    <br><br>
    <p>Yours in film,<br>
        SHORTLY</p>
@endsection

