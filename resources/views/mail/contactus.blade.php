@extends('layouts.mail')

@section('content')
    <h1>{{__("Incoming")}}</h1>
    <p>
        <strong>From:</strong>{{ $email }}<br>
        <strong>Name:</strong>{{ $name }}<br>
        <strong>Subject:</strong>{{ $subject }}<br>
        <p>
            {{ $message_text }}
        </p>
    </p>
@endsection