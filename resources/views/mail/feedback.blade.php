<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Feedback</title>
    <style>
        blockquote{
            background: #FEFEFE;
            border:1px solid #CECECE;
            border-radius:10px;
            padding:20px;
        }
    </style>
</head>
<body>
    <strong>Email:</strong> {{ $email }}<br><br>
    <strong>Feedback:</strong>
    <p>
    <blockquote>{{ $message_string }}</blockquote>
    </p>
</body>
</html>