@extends('layouts.clear')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{__('Tag')}}: {{ $tag->name }}
                    </div>
                    <div class="panel-body">
                        <div class="container-fluid">
                            <div class="row">
                                @foreach($tag->videos as $video)
                                    <div class="col-md-3">
                                        <div class="video-wrapper">
                                            @if(!auth()->check() || auth()->check() && auth()->user()->role == 'Filmmaker' && $video->video->user_id != auth()->user()->id)
                                                <a href="#" data-toggle="modal" data-target="#registerScreen">
                                            @else
                                                <a href="/video/{{$video->video->id}}">
                                            @endif
                                                <div class="video-image">
                                                    @if($video->main_thumb_url)
                                                        <img src="{{ $video->main_thumb_url}}" style="width:100%;" class="img-responsive" />
                                                    @else
                                                        @if(\Illuminate\Support\Facades\Storage::disk('s3thumbs')->exists($video->video->aws_prefix_folder."720p_16-9__-00001.png"))  <img src="{{  \Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($video->video->aws_prefix_folder."720p_16-9__-00002.png") }}" style="width:100%;" class="img-responsive"> @endif
                                                    @endif

                                                </div>
                                            </a>
                                            <div class="clearfix"></div>
                                            <div class="video-title">
                                                <h4>
                                                    @if(!auth()->check() || auth()->check() && auth()->user()->role == 'Filmmaker' && $video->vide->user_id != auth()->user()->id)
                                                        <a href="#" data-toggle="modal" data-target="#registerScreen">
                                                    @else
                                                        <a href="/video/{{$video->video->id}}">
                                                    @endif
                                                        {{ $video->video->title }}
                                                    </a>
                                                </h4>
                                            </div>
                                            <p>
                                                {{__('Uploaded')}}: {{ $video->video->created_at }}
                                            </p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="text-center">
                                {{--{{ $category->videos->links() }}--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection