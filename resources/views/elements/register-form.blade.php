<form method="POST" action="/register">
  {{ csrf_field() }}
  <input id="role" type="hidden" class="form-control" name="role" value="{{ request()->role }}" required>

  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
    <label for="name" class="auth-label">
        {{__('First name')}}
    </label>
    @if ($errors->has('name'))
        <span class="help-block">
        <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
  </div>

  <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">

    <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" required>
    <label for="lastname" class="auth-label">
        {{__('Last name')}}
    </label>
    @if ($errors->has('lastname'))
        <span class="help-block">
        <strong>{{ $errors->first('lastname') }}</strong>
        </span> 
    @endif
  </div>

  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
    <label for="email" class="auth-label">
        {{__('Email')}}
    </label>
    @if ($errors->has('email'))
        <span class="help-block">
        <strong>{{ $errors->first('email') }}</strong>
        </span> 
    @endif
  </div>

  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <input id="password" type="password" class="form-control" name="password" required>
    <label for="password" class="auth-label">
        {{ __('Password') }}
    </label>
    @if ($errors->has('password'))
        <span class="help-block">
        <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
  </div>

  <div class="form-group">
    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
    <label for="password-confirm" class="auth-label">
    {{__('Confirm your password')}}
    </label>
</div>

  <div class="form-group submit-wrap">
    <button type="submit" class="btn btn-gradiented btn-block">
      {{__('Create account')}}
    </button>
  </div>
</form>