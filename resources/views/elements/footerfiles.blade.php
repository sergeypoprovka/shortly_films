        

        <script type="text/javascript" src="{{ URL::asset('js/plugins/jquery/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/plugins/jquery/jquery-ui.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap.min.js')}}"></script>                
        <!-- END PLUGINS -->
        
        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src="{{ URL::asset('js/plugins/icheck/icheck.min.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
        
        <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>                
        <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>
        <!-- END THIS PAGE PLUGINS -->       
        
        <!-- START TEMPLATE -->
        
        <script type="text/javascript" src="{{ URL::asset('js/plugins.js')}}"></script>        
        <script type="text/javascript" src="{{ URL::asset('js/actions.js')}}"></script> 
        <script type="text/javascript" src="{{ URL::asset('js/status.js')}}"></script> 

        <script src="/js/app.js"></script>

        @yield('js')

        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->