 <ul class="x-navigation">
                    <li class="logo text-center">
                        <a href="/"><img height="43px" src="/img/logo.png"> </a>
                        {{--<a href="#" class="x-navigation-control"></a>--}}
                    </li>
                    @if(Auth::check())
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="{{ (!empty(session('image'))) ? '' : asset('img/user.jpg') }}" alt="User"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="{{ (!empty(session('image'))) ? '' : asset('img/user.jpg') }}" alt="User"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">{{ Auth::user()->name }}</div>
                                <div class="profile-data-title">{{ Auth::user()->role }}</div>
                            </div>
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    <li class=""><a href="/admin/videos">{{__('Videos')}}</a></li>
                        @if(Auth::user()->role == "Administrator")
                            <li class=""><a href="/admin/timers">{{__('Timers')}}</a></li>
                            <li class=""><a href="/admin/categories">{{__('Categories')}}</a></li>
                            <li class=""><a href="/admin/users">{{__('Users')}}</a></li>
                        @endif
                    @endif
                    <!-- <li class="xn-openable">
                        <a href="#"><span class="fa fa-dashboard"></span> <span class="xn-text">Dashboards</span></a>
                        <ul>
                            <li><a href="#"><span class="xn-text">Dashboard 1</span></a></li>
                        </ul>
                    </li> -->
            </ul>

                    
                <!-- END X-NAVIGATION -->