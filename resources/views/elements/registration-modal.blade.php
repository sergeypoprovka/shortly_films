<div class="modal fade" id="registerScreen" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" id="auth-menu">
        <div class="modal-content">
            <div class="modal-body">
                @if(!auth()->check())
                    <registration></registration>
                @else
                    <registration :ut="'FM'"></registration>
                @endif
            </div>
        </div>
    </div>
</div>