<div class="bottom-nav">
  <ul class="list-unstyled list-inline text-center">
    <li><a href="/faq">{{__('FAQ')}}</a></li>
    <li><a href="/about">{{__('About')}}</a></li>
    {{--<li><a target="blank" href="https://soapbox.shortly.film/soapbox/">{{__('Soapbox')}}</a></li>--}}
    <li><a href="/privacypolicy">{{__('Privacy policy')}}</a></li>
    <li><a href="/termsfm">{{__('Terms Filmmaker')}}</a></li>
    <li><a href="/termsfw">{{__('Terms Film watcher')}}</a></li>
    {{--<li><a href="/cookies">{{__('Cookies')}}</a></li>--}}
    <li><a href="/contact">{{__('Contact')}}</a></li>
  </ul>
  <div class="accounts-wrap text-center center-block">
  <a href="https://www.facebook.com/shortly.film" class="fa fa-facebook" target="blank"></a>
  <a href="https://www.instagram.com/shortly.film/" class="fa fa-instagram" target="blank"></a>
  {{--<a class="contant-email " href="mailto:hello@shortly.film">Contact us: hello@shortly.film</a>--}}
  </div>
  <div class="text-center center-block">
  © 2018 Shortly AB
  </div>
  
</div>