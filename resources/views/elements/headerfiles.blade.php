

        <!-- META SECTION -->
        <title>SHOTLY</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
                        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/theme-default.css')}}"/>
        <link rel="stylesheet" href="/css/video-js.css" />
        <script src="https://vjs.zencdn.net/5.19/video.min.js"></script>
        <script src="/js/videojs-resolution-switcher/lib/videojs-resolution-switcher.js"></script>
        <style>
                .pagination>.active>span{background-color:#33414e;border-color:#33414e;color:#FFF;}
                .pagination>li{display: inline-block;}
                ul.pagination{text-align: center;display: block;}
        </style>
        <!-- EOF CSS INCLUDE -->        