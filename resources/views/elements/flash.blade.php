@if(Session::has('success') || Session::has('danger'))
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
            @endif

            @if(Session::has('danger'))
                <p class="alert alert-danger">{{ Session::get('danger') }}</p>
            @endif
        </div>
    </div>
</div>
@endif