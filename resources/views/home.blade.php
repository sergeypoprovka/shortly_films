@extends('layouts.app')
    @section('content')

<div class="container-fluid">
  <div class="row">
    <div class="filter-videos">
      <div class="timer-filter pull-left">
        <ul class="timer-filter-list">
          <li>{{__('I have')}}</li>
          <li>
            <time-interval-filter :to_kill="'{{ request()->time ? request()->time : " 1 "}}'"></time-interval-filter>
          </li>
          <li>{{__('to enjoy')}}</li>
        </ul>
      </div>
      <div class="filter-count-videos pull-left">
        <span class="count">{{ $videos->total() }}</span> {{__('results')}}
      </div>
      <div class="filter pull-right">
        <filter-films :genre="'{{ request()->genre ? request()->genre : " " }}'" :range="'{{ request()->range ? request()->range : " newest " }}'"></filter-films>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="videos-container">
        @if(count($videos) > 0) @foreach($videos as $video)
        <div class="video-item">
            <div class="video-wrapper">
                @if(!auth()->check() || auth()->check() && auth()->user()->role == 'Filmmaker' && $video->user_id != auth()->user()->id)
                    <a href="#" data-toggle="modal" data-target="#registerScreen">
                @else
                    <a href="/video/{{$video->id}}">
                @endif
                    <div class="video-image" style="background-image:@if($video->main_thumb_url)
                            url('{{ $video->main_thumb_url}}');
                        @else
                            url( {{ (\Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($video->aws_prefix_folder.'720p_16-9__-00001.png')) ? \Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($video->aws_prefix_folder.'720p_16-9__-00002.png') : ''}}
                        @endif
                        ">
                        <div class="timer">
                            <img src="/img/timer_icon.svg" /> {{ date('i:s',strtotime($video->videoDuration)) }}
                        </div>
                    </div>
                </a>
                <div class="clearfix"></div>
                <div class="video-title">
                <h4>
                    @if(!auth()->check() || auth()->check() && auth()->user()->role == 'Filmmaker' && $video->user_id != auth()->user()->id)
                        <a href="#" data-toggle="modal" data-target="#registerScreen">
                    @else
                        <a href="/video/{{$video->id}}">
                    @endif
                        {{ $video->title }}
                    </a>
                </h4>
                </div><!-- .video-title -->
                
                <p class="video-author">
                    @if(isset($video->user->profile->image))
                        <img src="{{ $video->user->profile->image->profile->url }}" class="avatar-small" />
                    @else
                        <img src="https://placehold.it/48x48" class="avatar-small" />
                    @endif
                <a href="/author/{{ $video->user_id }}"> {{ $video->user->profile->name }} {{ $video->user->profile->lastname }}</a>
                </p>
            </div><!-- .video-wrapper -->
        </div><!-- .col-sm-6 .col-md-3 -->
        @endforeach
            <div class="text-center col-xs-12">
            {{ $videos->appends(Request::only('time')) ->appends(Request::only('range')) ->appends(Request::only('genre')) ->links() }}
            </div><!-- .text-center .col-xs-12 -->
        @else
            <div class="col-md-12">
            {{__('No films found.')}}
            </div><!-- .col-md-12 -->
        @endif
    </div><!-- .row -->
</div><!-- .container-fluid -->
@endsection