@extends('layouts.app')

@section('content')
    <div class="container follow-list">
        @foreach($follows as $follow)
            <div class="row">
                <div class="col-md-3">
                    <img src="{{ $follow->follower->profile->image->profile->url }}" class="img-circle" width="150px" />
                </div>
                <div class="col-md-6">
                    <div class="follow-info">
                        <h3><a href="/author/{{ $follow->follow_id }}">{{ $follow->follower->profile->name }} {{ $follow->follower->profile->lastname }}</a></h3>
                        <ul class="list-unstyled list-inline">
                            <li><img src="/img/videocam.svg" /> {{ count($follow->follower->videos) }}</li>
                            <li><img src="/img/person.svg" /> {{ count($follow->follower->follows) }}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 unsubscribe-button">
                    <form method="post" action="/unfollowUser/{{ $follow->id }}">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-unsubscribe">{{ __('Unfollow') }}</button>
                    </form>
                </div>
            </div>
        @endforeach
            <div class="clearfix"></div>
            <div class="text-center">
                {{ $follows->links() }}
            </div>
    </div>
@endsection