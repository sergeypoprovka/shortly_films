@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row profile-block">
                    <div class="col-md-3">
                        <div class="profile-block__info text-center">
                            <img src="{{ $user->profile->image->profile->url }}" class="img-circle" width="160px" />
                            <h3>{{ $user->profile->name }} {{ $user->profile->lastname }}</h3>
                            <div class="row text-center profile-block__info__stats">
                                <div class="col-xs-3 col-xs-offset-3"><img src="/img/videocam.svg" /> {{ count($user->videos) }}</div>
                                <div class="col-xs-3"><img src="/img/person.svg" />{{ count($user->followers) }}</div>
                            </div>
                            @auth
                                @if(auth()->user()->id != $user->id)
                                    <follow-user :follow_user="'{{ $user->id }}'"></follow-user>
                                @endif
                            @endauth
                        </div>
                    </div>
                    <div class="col-md-9">
                        @if($user->profile->description)
                        <div class="profile-block__about">
                            <h5>{{__('About')}}</h5>
                            <div class="profile-block__about__text">
                                {{ $user->profile->description }}
                            </div>
                        </div>
                        @endif
                        @if($user->profile->awards)
                        <div class="profile-block__awards">
                            <h5>{{__('Awards')}}</h5>
                            <div class="profile-block__awards__text">
                                {{ $user->profile->awards }}
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <hr>
                <div class="container-fluid">
                        @if(count($videos) > 0)
                            <div class="row">
                            @php $i=0; @endphp
                            @foreach($videos as $video)
                                @php $i+=1; @endphp
                                <div class="col-md-3">
                                    <div class="video-wrapper">
                                        @if(!auth()->check() || auth()->check() && auth()->user()->role == 'Filmmaker' && $video->user_id != auth()->user()->id)
                                            <a href="#" data-toggle="modal" data-target="#registerScreen">
                                        @else
                                            <a href="/video/{{$video->id}}">
                                        @endif
                                            <div class="video-image"style="background-image: url( {{ (\Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($video->aws_prefix_folder.'720p_16-9__-00002.png')) ? \Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($video->aws_prefix_folder.'720p_16-9__-00002.png') : ''}} ">
                                                <div class="timer">
                                                    <img src="/img/timer_icon.svg" /> {{ date('i:s',strtotime($video->videoDuration)) }}
                                                </div>
                                            </div>
                                        </a>
                                        <div class="clearfix"></div>
                                        <div class="video-title">
                                            <h4>
                                                @if(!auth()->check() || auth()->check() && auth()->user()->role == 'Filmmaker' && $video->user_id != auth()->user()->id)
                                                    <a href="#" data-toggle="modal" data-target="#registerScreen">
                                                @else
                                                    <a href="/video/{{$video->id}}">
                                                @endif
                                                    {{ $video->title }}
                                                </a>
                                            </h4>
                                        </div>
                                        <p class="video-author">
                                            @if(isset($video->user->profile->image))
                                                <img src="{{ $video->user->profile->image->profile->url }}" class="avatar-small" />
                                            @else
                                                <img src="https://placehold.it/48x48" class="avatar-small" />
                                            @endif
                                            <a href="/author/{{ $video->user_id }}"> {{ $video->user->profile->name }} {{ $video->user->profile->lastname }}</a>
                                        </p>
                                    </div>
                                </div>
                                    @if(($i) == count($videos)) </div> @elseif($i%4 == 0) </div><div class="row"> @endif
                            @endforeach
                            <div class="text-center">
                                {{ $videos->links() }}
                            </div>
                        @else
                            <div class="col-md-12">
                                {{__('No films found.')}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection