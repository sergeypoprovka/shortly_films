@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2>{{__('Reports by user')}}</h2>
                    </div>
                    <table class="table table-bordered table-striped table-responsive">
                        <tr>
                            <th>{{__('User')}}</th>
                            <th>{{__('Email')}}</th>
                            <th>{{__('Time')}}</th>
                            <th>{{__('Actions')}}</th>
                        </tr>
                        @if(count($users)>0)
                            @foreach($users as $user)
                                <tr>
                                    <td><a href="/admin/users/{{ $user->id }}">{{ $user->name }} {{ $user->lastname }}</a></td>
                                    <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                                    <td>{{ $user->sum }}</td>
                                    <td>
                                        <a href="/admin/reports/user/{{ $user->video_id }}" class="btn btn-warning btn-xs"><i class="fa fa-list"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td align="center" colspan="2">{{ __('No reports here') }}</td>
                            </tr>
                        @endif
                </div>
            </div>
        </div>
    </div>
@endsection