@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2>{{__('Reports by video')}}</h2>
                    </div>
                    <table class="table table-bordered table-striped table-responsive">
                        <tr>
                            <th>{{__('Video')}}</th>
                            <th>{{__('Time')}}</th>
                        </tr>
                        @if(count($videos) > 0)
                            @foreach($videos as $video)
                            <tr>
                                <td><a href="/video/{{ $video->video_id }}" target="_blank">{{ $video->video->title }}</a></td>
                                <td><strong>{{ $video->time_watched }}</strong>s</td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td align="center" colspan="2">{{ __('No reports here') }}</td>
                            </tr>
                        @endif
                    </table>
                    <div class="clearfix"></div>
                    <div class="pull-right">
                        <a href="{{ URL::previous() }}" class="btn btn-success btn-sm">{{__('Go back')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection