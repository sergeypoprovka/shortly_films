@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2>{{__('Reports by film')}}</h2>
                    </div>
                    <table class="table table-bordered table-striped table-responsive">
                        <tr>
                            <th>{{__('Video')}}</th>
                            <th>{{__('Time')}}</th>
                            <th>{{__('Actions')}}</th>
                        </tr>
                        @if(count($videos) > 0)
                            @foreach($videos as $video)
                            <tr>
                                <td><a href="/video/{{ $video->video_id }}" target="_blank">{{ $video->video->title }}</a></td>
                                <td><strong>{{ $video->sum }}</strong>s</td>
                                <td>
                                    <a href="/admin/reports/video/{{ $video->video_id }}" class="btn btn-warning btn-xs"><i class="fa fa-list"></i> </a>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td align="center" colspan="2">{{ __('No reports here') }}</td>
                            </tr>
                        @endif
                    </table>
                    <div class="pagination">
                        {{ $videos->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection