@extends('adminlte::page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2>{{__('Timers')}}</h2>
                    </div>
                        <table class="table table-bordered table-striped table-responsive">
                            <tr>
                                <th>{{__('User')}}</th>
                                <th>{{__('Film')}}</th>
                                <th>{{__('Time')}}({{__('seconds')}})</th>
                                <th>{{__('Date')}}</th>
                                <th>{{__('Actions')}}</th>
                            </tr>
                            @if(count($timers)==0)
                                <tr>
                                    <td align="center" colspan="4">{{ __('No timers') }}</td>
                                </tr>

                            @else
                                @foreach($timers as $timer)
                                    <tr>
                                        <td><a href="/admin/users/{{ $timer->user->id }}" target="_blank">{{ $timer->user->profile->name }} {{ $timer->user->profile->lastname }}</a></td>
                                        <td><a href="/video/{{ $timer->video->id }}" target="_blank">{{ $timer->video->title }}</a></td>
                                        <td>{{ $timer->time_watched }}</td>
                                        <td>{{ $timer->created_at }}<strong> ({{ \Carbon\Carbon::createFromTimestamp(strtotime($timer->created_at))->diffForHumans() }})</strong></td>
                                        <td>
                                            {{--<a href="/admin/timers/{{ $timer->id }}/edit" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>--}}
                                            <form style="display:inline" method="post" action="/admin/timers/{{ $timer->id }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-close"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    <div class="text-center">
                        {{ $timers->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection