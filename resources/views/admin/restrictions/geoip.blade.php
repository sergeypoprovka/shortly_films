@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2>{{__('GeoIP restrictions')}}</h2>
                        <div class="pull-right">
                            <form method="post" action="/admin/restrictions/geoip_store" class="form-inline">
                                {{ csrf_field() }}
                                <div class="form-group  ">
                                    <label class="control-label" for="country">{{__('Country')}}</label>
                                    <select name="country" class="form-control">
                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                    <button type="submit" class="btn btn-success">{{__('Restrict access')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br>
                    <table class="table table-striped table-responsive table-bordered">
                        <tr>
                            <th>{{__('Country name')}}</th>
                            <th>{{__('Actions')}}</th>
                        </tr>
                        @if(count($restricted_countries) > 0)
                            @foreach($restricted_countries as $restricted_country)
                                <tr>
                                    <td>{{ $restricted_country->country->name }}</td>
                                    <td>
                                        <form method="post" action="/admin/restrictions/geoip_destroy/{{ $restricted_country->id }}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="2" class="text-center">{{__('All countries allowed')}}</td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection