@extends('adminlte::page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <div class="pull-right"><a class="btn btn-success" href="/admin/users/create">{{ __('Create user') }}</a></div>
                        <h2>{{__('Users')}}</h2>
                        <div class="box box-info">
                            <div class="box-body">
                                <table class="table table-responsive">
                                    <tr>
                                        <th class="text-center">{{__('Total registered')}}</th>
                                        <th class="text-center">{{__('Total admins')}}</th>
                                        <th class="text-center">{{__('Total users')}}</th>
                                        <th class="text-center">{{__('Total FW')}}</th>
                                        <th class="text-center">{{__('Total FM')}}</th>
                                        <th class="text-center">{{__('Total FM(payed)')}}</th>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><h1 style="margin:0px;">{{ $countusers }}</h1></td>
                                        <td class="text-center"><h1 style="margin:0px;">{{ $countadmins }}</h1></td>
                                        <td class="text-center"><h1 style="margin:0px;">{{ $countfws }}</h1></td>
                                        <td class="text-center"><h1 style="margin:0px;">{{ $countfws_payed }}</h1></td>
                                        <td class="text-center"><h1 style="margin:0px;">{{ $countfms }}</h1></td>
                                        <td class="text-center"><h1 style="margin:0px;">{{ $countfms_payed }}</h1></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        {{--<div class="pull-right">--}}
                            {{--<a href="/admin/users/create" class="btn btn-success btn-lg"><i class="fa fa-plus"></i> {{__('Add user')}} </a>--}}
                        {{--</div>--}}
                    </div>
                    <hr>
                        <table class="table table-bordered table-striped table-responsive">
                            <tr>
                                <th>{{__('Name')}}</th>
                                <th>{{__('E-mail')}}</th>
                                <th>{{__('Role')}}</th>
                                <th>{{__('Payed')}}</th>
                                <th>{{__('Films uploaded')}}</th>
                                <th>{{__('Actions')}}</th>
                            </tr>
                            @if(count($users)==0)
                                <tr>
                                    <td align="center" colspan="4">{{ __('No users') }}</td>
                                </tr>

                            @else
                                @foreach($users as $user)
                                    <tr>
                                        <td>@if($user->profile && $user->profile->name && $user->profile->lastname) {{ $user->profile->name }} {{ $user->profile->lastname }}@endif</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->role }}</td>
                                        <td>
                                            @if($user->card_last_four != NULL)
                                                <i class="text-success fa fa-check"></i>
                                            @else
                                                <i class="text-danger fa fa-close"></i>
                                            @endif
                                        </td>
                                        <td>{{ count($user->videos) }}</td>
                                        <td>
                                            <a href="/admin/users/{{ $user->id }}" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a>
                                            <a href="/admin/users/{{ $user->id }}/edit" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
                                            <form style="display:inline" method="post" action="/admin/users/delete/{{ $user->id }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-close"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    <div class="text-center">
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection