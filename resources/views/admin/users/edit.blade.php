@extends('adminlte::page')

@section('content')
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <h2>{{__('Add user')}}</h2>
                <form method="post" action="/admin/users/{{ $user->id }}">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label" for="name">{{__('First name')}}</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $user->profile->name }}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="lastname">{{__('Last name')}}</label>
                        <input type="text" name="lastname" class="form-control" id="lastname" value="{{ $user->profile->lastname }}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="email">{{__('E-mail')}}</label>
                        <input type="text" name="email" class="form-control" id="email" value="{{ $user->email }}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="role">{{__('Role')}}</label>
                        <select class="form-control" name="role">
                            <option value="" selected disabled>{{__('Select role')}}</option>
                            <option @if($user->role == "User") selected @endif value="User">{{__('User')}}</option>
                            <option @if($user->role == "Administrator") selected @endif value="Administrator">{{__('Administrator')}}</option>
                            <option @if($user->role == "Filmmaker") selected @endif value="Filmmaker">{{__('Filmmaker')}}</option>
                            <option @if($user->role == "FullAccess") selected @endif value="FullAccess">{{__('Filmmaker(with benefits)')}}</option>

                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection