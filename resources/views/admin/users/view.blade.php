@extends('adminlte::page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2>{{ $user->name }}</h2>
                    </div>
                    <table class="table table-bordered table-striped table-responsive">
                        <tr>
                            <td>{{__('E-mail')}}</td>
                            <td>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <td>{{__('Role')}}</td>
                            <td>{{ $user->role }}</td>
                        </tr>
                        <tr>
                            <td>{{__('Registered at')}}</td>
                            <td>{{ $user->created_at }}</td>
                        </tr>
                    </table>
                    <div class="text-center">
                        <a href="{{ URL::previous() }}" class="btn btn-success btn-lg">{{__('Back')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection