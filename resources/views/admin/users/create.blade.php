@extends('adminlte::page')

@section('content')
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <h2>{{__('Add user')}}</h2>
                <form method="post" action="/admin/users">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label" for="name">{{__('Name')}}<sup>*</sup></label>
                        <input type="text" name="name" class="form-control" id="name" value="{{old('name')}}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="name">{{__('Lastname')}}<sup>*</sup></label>
                        <input type="text" name="lastname" class="form-control" id="lastname" value="{{old('lastname')}}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="email">{{__('E-mail')}}<sup>*</sup></label>
                        <input type="text" name="email" class="form-control" id="email" value="{{old('email')}}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="role">{{__('Role')}}<sup>*</sup></label>
                        <select class="form-control" name="role">
                            <option value="" selected disabled>{{__('Select role')}}</option>
                            <option value="User">{{__('User')}}</option>
                            <option value="Administrator">{{__('Administrator')}}</option>
                            <option value="Filmmaker">{{__('Filmmaker')}}</option>
                            <option value="FullAccess">{{__('Filmmaker(with benefits)')}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="password">{{__('Password')}}<sup>*</sup></label>
                        <input type="password" name="password" class="form-control" id="password" value="" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="password_confirmation">{{__('Password confirmation')}}<sup>*</sup></label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" value="" />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection