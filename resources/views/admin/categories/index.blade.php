@extends('adminlte::page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2>{{__('Categories')}}</h2>
                        <div class="pull-right">
                            <a href="/admin/categories/create" class="btn btn-success btn-lg"><i class="fa fa-plus"></i> {{__('Add category')}} </a>
                        </div>
                    </div>
                        <table class="table table-bordered table-striped table-responsive">
                            <tr>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Slug')}}</th>
                                <th>{{__('Parent')}}</th>
                                <th>{{__('Actions')}}</th>
                            </tr>
                            @if(count($categories)==0)
                                <tr>
                                    <td align="center" colspan="4">{{ __('No categories') }}</td>
                                </tr>

                            @else
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->slug }}</td>
                                        <td>{{ $category->parent }}</td>
                                        <td>
                                            <a href="/admin/categories/{{ $category->id }}/edit" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
                                            <form style="display:inline" method="post" action="/admin/categories/delete/{{ $category->id }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-close"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    <div class="text-center">
                        {{ $categories->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection