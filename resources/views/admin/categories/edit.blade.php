@extends('adminlte::page')

@section('content')
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <h2>{{__('Add category')}}</h2>
                <form method="post" action="/admin/categories/{{ $category->id }}/edit">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label" for="name">{{__('Name')}}</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $category->name }}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="slug">{{__('Slug')}}</label>
                        <input type="text" name="slug" class="form-control" id="slug" value="{{ $category->slug }}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parent">{{__('Parent')}}</label>
                        <input type="text" name="parent" class="form-control" id="parent" value="{{ $category->parent }}" />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection