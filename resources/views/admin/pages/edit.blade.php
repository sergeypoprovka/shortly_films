@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2>{{__('Edit page')}}</h2>
                    </div>
                    <form method="post" action="/admin/pages/{{ $staticPage->id }}">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label" for="title">{{__('Title')}}</label>
                            <input type="text" name="title" class="form-control" id="title" value="{{ $staticPage->title }}" />
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="content">{{__('Content')}}</label>
                            <textarea name="content" class="form-control" id="content">{{ $staticPage->content }}</textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">{{__('Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
        <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('content');
        </script>
@endsection