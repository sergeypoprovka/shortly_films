@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2>{{__('Pages')}}</h2>
                    </div>
                    <table class="table table-bordered table-striped table-responsive">
                        <tr>
                            <th>{{__('Title')}}</th>
                            <th>{{__('Actions')}}</th>
                        </tr>
                        @if(count($pages)==0)
                            <tr>
                                <td align="center" colspan="4">{{ __('No pages') }}</td>
                            </tr>

                        @else
                            @foreach($pages as $page)
                                <tr>
                                    <td>{{ $page->title }}</td>
                                    <td>
                                        <a href="/admin/pages/{{ $page->id }}/edit" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                    <div class="text-center">
                        {{ $pages->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection