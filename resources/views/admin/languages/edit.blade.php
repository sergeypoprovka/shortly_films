@extends('adminlte::page')

@section('content')
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <h2>{{__('Edit language')}}</h2>
                <form method="post" action="/admin/languages/{{ $language->id }}">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label" for="name">{{__('Name')}}</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $language->name }}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="slug">{{__('ISO Code')}}</label>
                        <input type="text" name="code" class="form-control" id="code" value="{{ $language->iso_code }}" />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection