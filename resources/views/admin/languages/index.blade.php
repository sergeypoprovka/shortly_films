@extends('adminlte::page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2>{{__('Language')}}</h2>
                        <div class="pull-right">
                            <a href="/admin/languages/create" class="btn btn-success btn-lg"><i class="fa fa-plus"></i> {{__('Add language')}} </a>
                        </div>
                    </div>
                        <table class="table table-bordered table-striped table-responsive">
                            <tr>
                                <th>{{__('Name')}}</th>
                                <th>{{__('ISO Code')}}</th>
                                <th>{{__('Actions')}}</th>
                            </tr>
                            @if(count($languages)==0)
                                <tr>
                                    <td align="center" colspan="4">{{ __('No languages') }}</td>
                                </tr>

                            @else
                                @foreach($languages as $language)
                                    <tr>
                                        <td>{{ $language->name }}</td>
                                        <td>{{ $language->iso_code }}</td>
                                        <td>
                                            <a href="/admin/languages/{{ $language->id }}/edit" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
                                            <form style="display:inline" method="post" action="/admin/languages/{{ $language->id }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-close"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    <div class="text-center">
                        {{ $languages->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection