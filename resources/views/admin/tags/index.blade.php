@extends('adminlte::page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2>{{__('Tag')}}</h2>
                        <div class="pull-right">
                            <a href="/admin/tags/create" class="btn btn-success btn-lg"><i class="fa fa-plus"></i> {{__('Add tag')}} </a>
                        </div>
                    </div>
                        <table class="table table-bordered table-striped table-responsive">
                            <tr>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Actions')}}</th>
                            </tr>
                            @if(count($tags)==0)
                                <tr>
                                    <td align="center" colspan="4">{{ __('No tags') }}</td>
                                </tr>

                            @else
                                @foreach($tags as $tag)
                                    <tr>
                                        <td>{{ $tag->name }}</td>
                                        <td>
                                            <a href="/admin/tags/{{ $tag->id }}/edit" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
                                            <form style="display:inline" method="post" action="/admin/tags/{{ $tag->id }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-close"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    <div class="text-center">
                        {{ $tags->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection