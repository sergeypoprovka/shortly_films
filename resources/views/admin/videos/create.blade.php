@extends('adminlte::page')

@section('content')
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <h2>{{__('Add video')}}</h2>
                <form method="post" action="/admin/videos" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label" for="title">{{__('Title')}}</label>
                        <input type="text" name="title" class="form-control" id="title" value="{{old('title')}}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="description">{{__('Description')}}</label>
                        <textarea name="description" class="form-control" id="description">{{old('description')}}</textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="video">{{__('Video')}}</label>
                        <input type="file" name="video" class="form-control" id="video" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="categories">{{__('Categories')}}</label>
                        <input type="text" name="categories" class="form-control" data-role="materialtags" id="categories" value="{{old('categories')}}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="tags">{{__('Tags')}}</label>
                        <input type="text" name="tags" class="form-control" data-role="materialtags" id="tags" value="{{old('tags')}}" />
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="/js/typeahead.js"></script>
    <script src="/js/materialize-tags.min.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=owyhqfmyc4ozprwugxlp7ubrfi0kp72n285obhnabnwjxolq"></script>
    <script>
        var tags = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url: '/fetchTags',
                filter: function(list) {
                    return $.map(list, function(tag) {
                        return { name: tag };
                    });
                }
            }
        });

        tags.initialize();

        $('#tags').materialtags({
            typeaheadjs: {
                name: 'tags',
                displayKey: 'name',
                valueKey: 'name',
                source: tags.ttAdapter()
            }
        });

        var categories = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url: '/fetchCategories',
                filter: function(list) {
                    return $.map(list, function(category) {
                        return { name: category };
                    });
                }
            }
        });

        categories.initialize();

        $('#categories').materialtags({
            typeaheadjs: {
                name: 'categories',
                displayKey: 'name',
                valueKey: 'name',
                source: categories.ttAdapter()
            }
        });

        tinymce.init({
            selector: '#description',
            height: 200,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'],
            paste_as_text: true,
            paste_auto_cleanup_on_paste : true,
            paste_remove_styles: true,
            paste_remove_styles_if_webkit: true,
            paste_strip_class_attributes: true,
            paste_text_sticky: true,
            valid_elements:"b, strong, i, underline, p, div, u"
        });
    </script>
@endsection