@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <h2>{{ $video->title }}</h2>
                        <div class="clearfix"></div>
                        <div>
                            <small>{{ $video->description }}</small>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="admin-video-original">
                        <video controls class="video-js vjs-default-skin vjs-big-play-centered" id="video-js"></video>
                    </div>
                    <hr>
                    <h2>{{__('Links')}}</h2>
                    <div class="admin-video-links">
                        <table class="table table-striped">
                            <tr>
                                <th>{{__('Type')}}</th>
                                <th>{{__('Link')}}</th>
                            </tr>
                            @if(count($links) > 0)
                                @if($links['original'])
                                <tr>
                                    <td><strong>Original</strong></td>
                                    <td>{{ $links['original'] }}</td>
                                </tr>
                                @endif
                                @if($links['1080p'])
                                <tr>
                                    <td><strong>1080p</strong></td>
                                    <td>{{ $links['1080p'] }}</td>
                                </tr>
                                @endif
                                @if($links['720p'])
                                <tr>
                                    <td><strong>720p</strong></td>
                                    <td>{{ $links['720p'] }}</td>
                                </tr>
                                @endif
                                @if($links['480p'])
                                <tr>
                                    <td><strong>480p</strong></td>
                                    <td>{{ $links['480p'] }}</td>
                                </tr>
                                @endif
                                @if($links['360p'])
                                <tr>
                                    <td><strong>360p</strong></td>
                                    <td>{{ $links['360p'] }}</td>
                                </tr>
                                @endif
                            @endif
                            <tr>
                                <td>Thumbnails</td>
                                <td>
                                    <admin-video-thumbs
                                            :video="{{ $video }}"
                                            :thumbs="{{ isset($links['thumbs']) ? json_encode($links['thumbs']) : array() }}">
                                    </admin-video-thumbs>
                                </td>
                            </tr>
                        </table>
                        <table class="table table-bordered table-striped table-responsive">
                            <tr>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Value')}}</th>
                            </tr>
                            <tr>
                                <td>{{__('Title')}}</td>
                                <td>{{ $video->title }}</td>
                            </tr>
                            <tr>
                                <td>{{__('Original Title')}}</td>
                                <td>{{ $video->original_title }}</td>
                            </tr>
                            <tr>
                                <td>{{__('Country')}}</td>
                                <td>{{ $video->countries->name }}</td>
                            </tr>
                            <tr>
                                <td>{{__('Language')}}</td>
                                <td>{{ $video->languages->name }}</td>
                            </tr>
                            <tr>
                                <td>{{__('Producer')}}</td>
                                <td>{{ $video->producer }}</td>
                            </tr>
                            <tr>
                                <td>{{__('Director')}}</td>
                                <td>{{ $video->director }}</td>
                            </tr>
                            @if($video->photo)
                            <tr>
                                <td>{{__('Photo')}}</td>
                                <td><img src="/uploads/videos/{{ $video->photo }}" height="200px" /></td>
                            </tr>
                            @endif
                            @if($video->actors)
                            <tr>
                                <td>{{__('Actors')}}</td>
                                <td>{{ $video->actors }}</td>
                            </tr>
                            @endif
                            @if($video->awards)
                            <tr>
                                <td>{{__('Awards')}}</td>
                                <td>{{ $video->awards }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td>{{__('Production year')}}</td>
                                <td>{{ $video->production_year }}</td>
                            </tr>
                            <tr>
                                <td>{{__('Release year')}}</td>
                                <td>{{ $video->release_year }}</td>
                            </tr>
                            @if($video->storyline)
                            <tr>
                                <td>{{__('Storyline')}}</td>
                                <td>{!! $video->storyline !!}</td>
                            </tr>
                            @endif
                            @if($video->script)
                            <tr>
                                <td>{{__('Script')}}</td>
                                <td>{!!  $video->script  !!}</td>
                            </tr>
                            @endif
                            @if($video->didyouknow)
                            <tr>
                                <td>{{__('Did you know')}}</td>
                                <td>{!! $video->didyouknow !!}</td>
                            </tr>
                            @endif
                            @if($video->soundtrack)
                            <tr>
                                <td>{{__('Soundtrack')}}</td>
                                <td>{{ $video->soundtrack }}</td>
                            </tr>
                            @endif
                            @if($video->styles != NULL)
                            <tr>
                                <td>{{__('Style')}}</td>
                                <td>{{ $video->styles->name }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td>{{__('Duration')}}</td>
                                <td>{{ $video->videoDuration }}</td>
                            </tr>
                            <tr>
                                <td>{{__('Categories')}}</td>
                                <td>@foreach($video->cats as $cat) {{ $cat->name }}, @endforeach</td>
                            </tr>
                            <tr>
                                <td>{{__('Tags')}}</td>
                                <td>@foreach($video->tags as $tag) {{ $tag->name }}, @endforeach</td>
                            </tr>
                            <tr>
                                <td>{{__('Keywords')}}</td>
                                {{--<td>@foreach($video->keywords as $kw) {{ $kw->name }}, @endforeach</td>--}}
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://vjs.zencdn.net/6.4.0/video.js"></script>
    <script src="/js/videojs-resolution-switcher/lib/videojs-resolution-switcher.js"></script>
    <script>
        var player = videojs('video-js', {
            controls: true,
            fluid: true,
            plugins: {
                videoJsResolutionSwitcher: {
                    default: '480',
                    dynamicLabel: true
                }
            }
        }, function(){
            player.updateSrc([
                {
                    src: '<?=$links['360p']?>',
                    type: 'video/mp4',
                    label: '360'
                },
                {
                    src: '<?=$links['480p']?>',
                    type: 'video/mp4',
                    label: '480'
                },
                {
                    src: '<?=$links['720p']?>',
                    type: 'video/mp4',
                    label: '720'
                },
                {
                    src: '<?=$links['1080p']?>',
                    type: 'video/mp4',
                    label: '1080'
                }
            ])

            player.on('resolutionchange', function(){
                //console.info('Source changed to %s', player.src())
            })

        })
    </script>
@endsection

@section('css')
    <style>
        .list-video-thumbs li a img{
            border: 10px solid transparent;
        }
        .list-video-thumbs li a.main img{
            border: 10px solid red;
        }
        .list-video-thumbs li a:hover img{
            border: 10px solid white;
        }
    </style>
    <link href="https://vjs.zencdn.net/6.4.0/video-js.css" rel="stylesheet">
@endsection
