@extends('adminlte::page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        {{--<div class="pull-right">--}}
                            {{--<a href="/admin/videos/create" class="btn btn-success btn-lg"><i class="fa fa-plus"></i> {{__('Add video')}} </a>--}}
                        {{--</div>--}}
                        <h2>{{__('Films')}}</h2>
                    </div>
                    <hr>
                        <table class="table table-bordered table-striped table-responsive">
                            <tr>
                                <th></th>
                                <th>{{__('Title')}}</th>
                                <th>{{__('User')}}</th>
                                <th>{{__('Date uploaded')}}</th>
                                <th>{{__('Time')}}</th>
                                <th>{{__('Actions')}}</th>
                            </tr>
                            @if(count($videos)==0)
                                <tr>
                                    <td align="center" colspan="4">{{ __('No films') }}</td>
                                </tr>

                            @else
                                @foreach($videos as $video)
                                    <tr>
                                        <td>@if($video->approved) <i class="fa text-success fa-check"></i> @else <i class="fa text-danger fa-close"></i> @endif</td>
                                        <td>{{ $video->title }}</td>
                                        <td>{{ $video->user->profile->name }} {{ $video->user->profile->lastname }}</td>
                                        <td>{{ $video->created_at }}</td>
                                        <td><strong>{{ $video->watched() }}</strong>s</td>
                                        <td>
                                            @if(!$video->approved)
                                                <form style="display:inline" method="post" action="/admin/videos/{{ $video->id }}/approve">
                                                    {{ csrf_field() }}
                                                    {{ method_field('POST') }}
                                                    <button type="submit" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Approve"><i class="fa fa-thumbs-up"></i></button>
                                                </form>
                                            @else
                                                <form style="display:inline" method="post" action="/admin/videos/{{ $video->id }}/unapprove">
                                                    {{ csrf_field() }}
                                                    {{ method_field('POST') }}
                                                    <button type="submit" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Deny"><i class="fa fa-thumbs-down"></i></button>
                                                </form>
                                            @endif
                                            <a href="/admin/videos/{{ $video->id }}" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a>
                                            {{--<a href="/admin/videos/{{ $video->id }}/edit" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>--}}
                                            <form style="display:inline" method="post" action="/admin/videos/delete/{{ $video->id }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-close"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    <div class="text-center">
                        {{ $videos->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection