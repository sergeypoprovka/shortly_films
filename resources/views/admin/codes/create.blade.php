@extends('adminlte::page')

@section('content')
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <h2>{{__('Add code')}}</h2>
                <form method="post" action="/admin/codes">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label" for="code">{{__('Code')}}</label>
                        <div class="row">
                            <div class="col-md-10">
                                <input type="text" name="code" class="form-control" id="code" value="{{old('code')}}" />
                            </div>
                            <div class="col-md-2 text-right">
                                <a onClick="generateCode(event)" href="#" class="btn btn-success">{{__('Generate code')}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="for_role">{{__('For role')}}</label>
                        <select name="for_role" class="form-control">
                            <option value="" selected disabled>{{__('Select role to apply')}}</option>
                            <option value="FilmMaker">{{__('Film Maker')}}</option>
                            <option value="FilmWatcher">{{__('Film Watcher')}}</option>
                            {{--<option value="VIP">{{__('VIP')}}</option>--}}
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="free_days">{{__('Free days')}}</label>
                        <input type="number" name="free_days" class="form-control" id="free_days" value="{{old('free_days')}}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="allow_multiple">{{__('Allow multiple usage')}}</label>
                        <select name="allow_multiple" class="form-control">
                            <option value="0" selected>{{__('No')}}</option>
                            <option value="1">{{__('Yes')}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        function generateCode(e){
            e.preventDefault();
            var field = $("#code");
            field.val(randString());
        }
        function randString(){
            var possible = '';
            possible += 'abcdefghijklmnopqrstuvwxyz';
            possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            possible += '0123456789';
            var text = '';
            for(var i=0; i < 10; i++) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return text;
        }
    </script>
@endsection