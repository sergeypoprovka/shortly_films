@extends('adminlte::page')

@section('content')
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <h2>{{__('Edit code')}}</h2>
                <form method="post" action="/admin/codes/{{ $code->id }}">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label" for="code">{{__('Code')}}</label>
                        <input type="text" name="code" class="form-control" id="code" value="{{ $code->code }}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="for_role">{{__('For role')}}</label>
                        <select name="for_role" class="form-control">
                            <option {{ $code->for_role == "FilmMaker" ? "selected" : "" }} value="FilmMaker">{{__('Film Maker')}}</option>
                            <option {{ $code->for_role == "FilmWatcher" ? "selected" : "" }} value="FilmWatcher">{{__('Film Watcher')}}</option>
                            {{--<option {{ $code->for_role == "VIP" ? "selected" : "" }} value="VIP">{{__('VIP')}}</option>--}}
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="free_days">{{__('Free days')}}</label>
                        <input type="number" name="free_days" class="form-control" id="free_days" value="{{ $code->free_days }}" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="allow_multiple">{{__('Allow multiple usage')}}</label>
                        <select name="allow_multiple" class="form-control">
                            <option {{ $code->allow_multiple == 0 ? "selected" : "" }} value="0">{{__('No')}}</option>
                            <option {{ $code->allow_multiple == 1 ? "selected" : "" }} value="1">{{__('Yes')}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection