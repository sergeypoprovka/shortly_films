@extends('adminlte::page')

@section('content')

    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="page-title">
                        <div class="pull-right">
                            <a href="/admin/codes/create" class="btn btn-success btn-lg"><i class="fa fa-plus"></i> {{__('Add code')}} </a>
                        </div>
                        <h2>{{__('Campaign codes')}}</h2>
                    </div>
                    <hr>
                        <table class="table table-bordered table-striped table-responsive">
                            <tr>
                                <th>{{__('Name')}}</th>
                                <th>{{__('For role')}}</th>
                                <th>{{__('Free days')}}</th>
                                <th>{{__('Multiple')}}</th>
                                <th>{{__('Used')}}</th>
                                <th>{{__('Actions')}}</th>
                            </tr>
                            @if(count($codes)==0)
                                <tr>
                                    <td align="center" colspan="6">{{ __('No codes') }}</td>
                                </tr>

                            @else
                                @foreach($codes as $code)
                                    <tr>
                                        <td>{{ $code->code }}</td>
                                        <td>{{ $code->for_role }}</td>
                                        <td>{{ $code->free_days }}</td>
                                        <td>{{ $code->allow_multiple }}</td>
                                        <td>{{ $code->used }}</td>
                                        <td>
                                            <a href="/admin/codes/{{ $code->id }}/edit" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
                                            <form style="display:inline" method="post" action="/admin/codes/{{ $code->id }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Remove"><i class="fa fa-close"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    <div class="text-center">
                        {{ $codes->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection