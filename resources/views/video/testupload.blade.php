@extends('layouts.app')

@section('content')
    <form method="post" action="/video/videoUpload" enctype="multipart/form-data">
        <input type="file" name="file" class="form-control" />
        <button type="submit">Send</button>
    </form>
@endsection