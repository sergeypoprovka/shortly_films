@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <video-upload></video-upload>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=owyhqfmyc4ozprwugxlp7ubrfi0kp72n285obhnabnwjxolq"></script>
    <script>
//        var tags = new Bloodhound({
//            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
//            queryTokenizer: Bloodhound.tokenizers.whitespace,
//            prefetch: {
//                url: '/fetchTags',
//                filter: function(list) {
//                    return $.map(list, function(tag) {
//                        return { name: tag };
//                    });
//                }
//            }
//        });
//
//        tags.initialize();
//
//        $('#tags').materialtags({
//            typeaheadjs: {
//                name: 'tags',
//                displayKey: 'name',
//                valueKey: 'name',
//                source: tags.ttAdapter()
//            }
//        });
//
//        var categories = new Bloodhound({
//            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
//            queryTokenizer: Bloodhound.tokenizers.whitespace,
//            prefetch: {
//                url: '/fetchCategories',
//                filter: function(list) {
//                    return $.map(list, function(category) {
//                        return { name: category };
//                    });
//                }
//            }
//        });
//
//        categories.initialize();
//
//        $('#categories').materialtags({
//            typeaheadjs: {
//                name: 'categories',
//                displayKey: 'name',
//                valueKey: 'name',
//                source: categories.ttAdapter()
//            }
//        });

        tinymce.init({
            selector: '.editable-area',
            height: 200,
            menubar: false,
            skin_url:"/css/custom",
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'],
            paste_as_text: true,
            paste_auto_cleanup_on_paste : true,
            paste_remove_styles: true,
            paste_remove_styles_if_webkit: true,
            paste_strip_class_attributes: true,
            paste_text_sticky: true,
            valid_elements:"b, strong, i, underline, p, u"
        });
    </script>
@endsection

{{--<div class="form-group{{ $errors->has('video') ? ' has-error' : '' }}">--}}
{{--<label class="control-label" for="video">{{__('Video')}}</label>--}}
{{--<input type="file" name="video" class="form-control" id="video" />--}}
{{--@if ($errors->has('video'))--}}
{{--<span class="help-block">--}}
{{--<strong>{{ $errors->first('video') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}