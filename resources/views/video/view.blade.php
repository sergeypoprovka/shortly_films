@extends('layouts.app')

@section('content')
    @can('show', $video)
    @if(!$restricted)
        <div class="container-fluid video-placeholder">
        <div class="video-video">
                <video-like-later :video="'{{$video->id}}'" :liked="'{{ $video->likedByUser() != 0 ? true : false }}'" :watchlatered="'{{ $video->addedToWatchLaterByUser() != 0 ? true : false }}'"></video-like-later>
                <video-player
                    :p360="'{{ $links['360p'] }}'"
                    :p480="'{{ $links['480p'] }}'"
                    :p720="'{{ $links['720p'] }}'"
                    :p1080="'{{ $links['1080p'] }}'"
                    :video="{{ $video->id }}"
                    :time="{{ !empty($video->videotimer) ? $video->videotimer : "0"}}"
                    :track="'{{ auth()->user()->id == $video->user_id || auth()->user()->role == 'Administrator' ? 'false' : 'true' }}'"
                    ></video-player>
        </div>
    </div>
    <div class="container video-info">
        <div class="row">
            <nav class="video-nav">
                <h2 class="video-title">{{ $video->title }}</h2>
                <div class="video-author">
                    <div class="author-avatar">
                        @if(isset($video->user->profile->image))
                            <img src="{{ $video->user->profile->image->profile->url }}" class="avatar-medium" />
                        @else
                            <img src="https://placehold.it/48x48" class="avatar-medium" />
                        @endif
                    </div>
                
                    <a class="author-name" href="/author/{{ $video->user_id }}"> {{ $video->user->profile->name }} {{ $video->user->profile->lastname }}</a>
                </div>
                <rating :video="{{ $video->id }}" :video_owner="{{ $video->user_id }}" :user="{{ auth()->user()->id }}"></rating>
            </nav>
        </div>
        <div class="row">
            <hr class="video-info-divider">
            <div class="clearfix"></div>
            <div class="col-md-7 video-info">
                <div class="row">
                @if($video->script)
                <div class="video-description">
                    <h5>{{__('About')}}</h5>
                    <article class="video-description-text">{!!  $video->script  !!}</article>
                </div>
                @endif
                    @if($video->awards)
                        <div class="video-categories video-awards">
                            <h5>{{__('Awards')}}</h5>
                            <p>{{ $video->awards }}</p>
                        </div>
                    @endif
                @if($video->storyline)
                    <div class="video-description">
                        <h5>{{__('Storyline')}}</h5>
                        <article class="video-description-text">{!!  $video->storyline  !!}</article>
                    </div>
                @endif
                @if($video->didyouknow)
                    <div class="video-description">
                        <h5>{{__('Did you know')}}</h5>
                        <article class="video-description-text">{!!  $video->didyouknow  !!}</article>
                    </div>
                @endif
                @if(count($video->cats) > 0)
                <div class="video-description">
                    <h5>{{__('Genres')}}</h5>
                    <ul class="list-unstyled list-inline">
                    @foreach($video->cats as $category)
                        <li>
                            <a href="/category/{{ $category->id }}">{{ $category->name }}</a>
                        </li>
                    @endforeach
                    </ul>
                </div>
                @endif
                {{--<div id="disqus_thread"></div>--}}
                {{--<disqus></disqus>--}}
                {{--<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>--}}
                </div>
            </div>
            <div class="col-md-4 col-md-offset-1">
                <div class="row">
                @if($video->photo)
                <div class="video-poster">
                    <img src="/uploads/videos/{{$video->photo}}" class="img-responsive" />
                </div>
                @endif
                <div class="video-attributes">
                    <h5>Details</h5>
                    <ul class="list-unstyled">
                        @if($video->country)
                            <li>{{__('Country')}}: <span>{{ $video->countries->name }}</span></li>
                        @endif
                        @if($video->language)
                            <li>{{__('Language')}}: <span>{{ $video->languages->name }}</span></li>
                        @endif
                        @if($video->producer)
                            <li>{{__('Producer')}}: <span>{{ $video->producer }}</span></li>
                        @endif
                        @if($video->director)
                            <li>{{__('Director')}}: <span>{{ $video->director }}</span></li>
                        @endif
                        @if($video->production_year)
                            <li>{{__('Production year')}}: <span>{{ $video->production_year }}</span></li>
                        @endif
                        @if($video->release_year)
                            <li>{{__('Release year')}}: <span>{{ $video->release_year }}</span></li>
                        @endif
                        @if($video->soundtrack)
                            <li>{{__('Soundtrack')}}: <span>{{ $video->soundtrack }}</span></li>
                        @endif
                    </ul>
                </div>
                <div class="video-actors">
                    <h5>{{__('Actors')}}</h5>
                    <ul class="list-unstyled list-inline">
                        <li>{{ $video->actors }}</li>
                    </ul>
                </div>
                @if($video->tags)
                <div class="video-tags">
                    <h5>{{__('Tags')}}</h5>
                    <ul class="list-unstyled list-inline">
                        @foreach($video->tags as $tag)
                            <li>
                                <a href="/tag/{{ $tag->id }}">{{ $tag->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                @endif
                </div><!-- .row -->
                
            </div>
        </div>
    </div>
    @else
    <div class="container-fluid">
        <div class="row">

                <h4>{{__('We are sorry but this film is restricted to show in your country...')}}</h4>
                <br>
                <a href="/" class="btn btn-warning">{{__('Find another film')}}</a>

        </div>
    </div>
    @endif
    @endcan
    @cannot('show', $video)
        <div class="text-center">
            <h2>Sorry, but you can watch only your own videos...</h2>
        </div>
    @endcannot
@endsection

@section('js')
    {{--<script id="dsq-count-scr" src="//http-shortly-film.disqus.com/count.js" async></script>--}}
@endsection