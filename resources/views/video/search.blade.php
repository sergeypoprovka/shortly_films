@extends('layouts.app')

@section('content')
    <div class="container">
        {{--<h3 class="static-header">{{__('Films you love')}}</h3>--}}
        {{--<hr>--}}
        @if(count($results) > 0)
            @foreach($results as $item)
                <div class="row history-item">
                    <div class="col-md-3">
                        <div class="video-wrapper">
                            <a href="/video/{{$item->id}}">
                                <div class="video-image"style="background-image: @if($item->main_thumb_url)
                                        url('{{ $item->main_thumb_url}}');
                                @else
                                        url( {{ (\Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($item->aws_prefix_folder.'720p_16-9__-00002.png')) ? \Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($item->aws_prefix_folder.'720p_16-9__-00002.png') : ''}}
                                @endif
                                        ">
                                    <div class="timer">
                                        <img src="/img/timer_icon.svg" /> {{ date('i:s',strtotime($item->videoDuration)) }}
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="video-title"><h4><a href="/video/{{$item->id}}">{{ $item->title }}</a></h4></div>
                        <p class="video-author">
                            @if(isset($item->user->image))
                                <img src="{{ $item->user->image }}" class="avatar-small" />
                            @else
                                <img src="https://placehold.it/48x48" class="avatar-small" />
                            @endif
                            <a href="/author/{{ $item->user_id }}"> {{ $item->user->profile->name }} {{ $item->user->profile->lastname }}</a>
                        </p>
                    </div>
                </div>
            @endforeach
            <div class="text-center">
                {{ $results->appends(Request::only('q'))->links() }}
            </div>
        @else
            <p>Nothing found.</p>
        @endif
    </div>

@endsection
