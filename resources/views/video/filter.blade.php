@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(isset($user))
                    <h4 class="filter-page-title">{{__('Author')}}: {{ $user->profile->name }} {{ $user->profile->lastname }}</h4>
                @endif
                @if(isset($category))
                    <h4 class="filter-page-title">{{__('Category')}}: {{ $category->name }}</h4>
                @endif
                @if(isset($tag))
                    <h4 class="filter-page-title">{{__('Tag')}}: {{ $tag->name }}</h4>
                @endif


                        <div class="container-fluid">
                            <div class="row">
                                @if(count($videos) > 0)
                                    @foreach($videos as $video)
                                        <div class="col-md-3">
                                            <div class="video-wrapper">
                                                <a href="/video/{{$video->id}}">
                                                    <div class="video-image"style="background-image: @if($video->main_thumb_url)
                                                            url('{{ $video->main_thumb_url}}');
                                                    @else
                                                            url( {{ (\Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($video->aws_prefix_folder.'720p_16-9__-00002.png')) ? \Illuminate\Support\Facades\Storage::disk('s3thumbs')->url($video->aws_prefix_folder.'720p_16-9__-00002.png') : ''}}
                                                    @endif
                                                            ">
                                                        <div class="timer">
                                                            <img src="/img/timer_icon.svg" /> {{ date('i:s',strtotime($video->videoDuration)) }}
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="clearfix"></div>
                                                <div class="video-title"><h4><a href="/video/{{$video->id}}">{{ $video->title }}</a></h4></div>
                                                <p class="video-author">
                                                    @if(isset($video->user->image))
                                                        <img src="{{ $video->user->image }}" class="avatar-small" />
                                                    @else
                                                        <img src="https://placehold.it/48x48" class="avatar-small" />
                                                    @endif
                                                    <a href="/author/{{ $video->user_id }}"> {{ $video->user->profile->name }} {{ $video->user->profile->lastname }}</a>
                                                </p>
                                            </div>
                                        </div>
                                    @endforeach
                            <div class="text-center">
                                {{ $videos->links() }}
                            </div>
                            @else
                                <div class="col-md-12">
                                    {{__('No films found.')}}
                                </div>
                            @endif
                            </div>
                            </div>
                </div>
            </div>
        </div>
@endsection