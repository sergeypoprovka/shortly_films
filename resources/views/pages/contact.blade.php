@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>{{__('Contact us')}}</h1>
                <p>
                    <form method="post" action="/contact" class="contact-us-form">
                        {{ csrf_field() }}
                        <div class="form-group @if($errors->has('name')) has-error @endif">
                            <label class="control-label" for="name">{{__('Full name')}}</label>
                            <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" />
                            @if($errors->has('name'))
                                <span class="text text-danger">{{ $errors->first('name') }}</span>
                            @endif

                        </div>
                        <div class="form-group @if($errors->has('email')) has-error @endif">
                            <label class="control-label" for="email">{{__('Email')}}</label>
                            <input type="text" name="email" class="form-control" id="email" value="{{ old('email') }}" />
                            @if($errors->has('email'))
                                <span class="text text-danger">{{ $errors->first('email') }}</span>
                            @endif

                        </div>
                        <div class="form-group @if($errors->has('subject')) has-error @endif">
                            <label class="control-label" for="subject">{{__('Subject')}}</label>
                            <input type="text" name="subject" class="form-control" id="subject" value="{{ old('subject') }}" />
                            @if($errors->has('subject'))
                                <span class="text text-danger">{{ $errors->first('subject') }}</span>
                            @endif

                        </div>
                        <div class="form-group @if($errors->has('message')) has-error @endif">
                            <label class="control-label" for="message">{{__('Message')}}</label>
                            <textarea name="message" id="message" rows="5" class="form-control">{{ old('message') }}</textarea>
                            @if($errors->has('message'))
                                <span class="text text-danger">{{ $errors->first('message') }}</span>
                            @endif

                        </div>
                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-lg btn-warning"><i class="fa fa-envelope"></i> {{__('Send')}}</button>
                        </div>
                    </form>
                </p>
            </div>
        </div>
    </div>
@endsection