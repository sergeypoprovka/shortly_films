@extends('layouts.plans')

@section('content')
    

    <subscription-chooser inline-template>
        <div>
        <div class="shortly-video-overview col-xs-12 col-md-8 col-md-offset-2 text-center">
        <div class="btn btn-warning preview-shortly" @click="toggleShortlyPreview">
            <span v-if="!this.showShortlyPreview">Get a preview of Shortly</span>
            <span v-else>Close preview</span>
        </div>
            <video src="/video/Shortly.mp4" controls class="col-xs-12" v-if="showShortlyPreview"></video>
        </div>
            {{--<div class="text-center" style="margin-top:40px;" v-if="!chosenPlan">--}}
                {{--<a @click.prevent="campaignStart" href="#" class="btn btn-warning">{{__('I have a Campaign code and want to become a Film Maker')}}</a><br>--}}
                {{--<h3>OR</h3>--}}
            {{--</div>--}}
            {{--<div class="choose-term" v-if="!chosenPlan">--}}
                {{--<ul class="nav nav-pills">--}}
                    {{--<li role="presentation" class="active"><a href="#monthly" @click.prevent="showMonthly()" class="showMonthly">{{__('Billed monthly')}}</a></li>--}}
                    {{--<li role="presentation"><a href="#annually" @click.prevent="showAnnual()" class="showAnnually">{{__('Billed annually - Save 20%')}}</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
            
            <div class="tariffs">
                <div class="choose-plan container-fluid" v-show="!chosenPlan">
                <div class="row" id="monthly">
                    <div class="col-sm-6 col-md-4 col-md-offset-2 col-lg-3 col-lg-offset-3 tarif-box-wrap">
                        <div class="tarif-box equalHeight">
                            <div class="text-center">
                                <img src="/img/filmwatcher_icon.svg" />
                                <h2>{{__('Film watcher')}}</h2>
                                <div class="price">
                                    <span>&euro;9</span>
                                    {{__('per month')}}
                                </div>
                                <div class="choose-term" v-if="!chosenPlan">
                                    <ul class="nav nav-pills">
                                        <li role="presentation" class="active"><a href="#monthly" @click.prevent="showMonthly()" class="showMonthly">{{__('Monthly')}}</a></li>
                                        <li role="presentation"><a href="#annually" @click.prevent="showAnnual()" class="showAnnually">{{__('Annually')}}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <ul class="features list-unstyled">
                                <li>Access all content</li>
                                <li>Sort content by genre, filmmaker and viewing time</li>
                                <li>Create personal viewing lists</li>
                                <li>Rate and comment on films</li>
                                <li>Follow your favourite channels</li>
                            </ul>
                            <div class="text-center">
                                <a @click.prevent="subscribe('watcher_monthly')" href="#" class="btn btn-warning"><span>{{__('Sign me up!')}}</span><div v-if="loading" class="loading"></div></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3 tarif-box-wrap">
                        <div class="tarif-box equalHeight">
                            <div class="text-center">
                                <img src="/img/filmmaker_icon.svg" />
                                <h2>{{__('Filmmaker')}}</h2>
                                <div class="price">
                                    <span>FREE</span>
                                </div>
                            </div>
                            <ul class="features list-unstyled">
                                <li>Upload unlimited films (up to 1TB)</li>
                                <li>Receive royalties when your films are viewed</li>
                                <li>Creat your own channel</li>
                                <li>Build communities</li>
                                <li>Showcase your art</li>
                                <li>Get statistics and data from Shortly’s database</li>
                                <li>Receive global exposure</li>
                                <li>Access all content</li>
                                <li>Sort content by genre, filmmaker, and viewing time</li>
                                <li>Creat personal viewing list</li>
                                <li>Rate and comment on films</li>
                            </ul>
                            <div class="text-center">
                                <a @click.prevent="subscribe('maker_monthly')" href="#" class="btn btn-warning"><span>{{__('Sign me up!')}}</span><div v-if="loading" class="loading"></div></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="annually">
                    <div class="col-sm-6 col-md-4 col-md-offset-2 col-lg-offset-3 col-lg-3 tarif-box-wrap" disabled>
                        <div class="tarif-box equalHeight">
                            <div class="text-center">
                                <img src="/img/filmwatcher_icon.svg" />
                                <h2>{{__('Film watcher')}}</h2>
                                <div class="price">
                                    <span>&euro;90</span>
                                    {{__('per year')}}
                                </div>
                                <div class="choose-term" v-if="!chosenPlan">
                                    <ul class="nav nav-pills">
                                        <li role="presentation" class="active"><a href="#monthly" @click.prevent="showMonthly()" class="showMonthly">{{__('Monthly')}}</a></li>
                                        <li role="presentation"><a href="#annually" @click.prevent="showAnnual()" class="showAnnually">{{__('Annually')}}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <ul class="features list-unstyled">
                                <li>Access all content</li>
                                <li>Sort content by genre, filmmaker and viewing time</li>
                                <li>Create personal viewing lists</li>
                                <li>Rate and comment on films</li>
                                <li>Follow your favourite channels</li>
                                <li>Skip the ads</li>
                            </ul>
                            <div class="text-center">
                                <a @click.prevent="subscribe('watcher_yearly')" href="#" class="btn btn-warning"><span>{{__('Sign me up!')}}</span><div v-if="loading" class="loading"></div></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3 tarif-box-wrap">
                        <div class="tarif-box equalHeight">Choose your destiny
                            <div class="text-center">
                                <img src="/img/filmmaker_icon.svg" />
                                <h2>{{__('Filmmaker')}}</h2>
                                <div class="price">
                                    <span>FREE</span>
                                </div>
                            </div>
                            <ul class="features list-unstyled">
                                <li>Upload unlimited films (up to 1TB)</li>
                                <li>Receive royalties when your films are viewed</li>
                                <li>Creat your own channel</li>
                                <li>Build communities</li>
                                <li>Showcase your art</li>
                                <li>Get statistics and data from Shortly’s database</li>
                                <li>Receive global exposure</li>
                                <li>Access all content</li>
                                <li>Sort content by genre, filmmaker, and viewing time</li>
                                <li>Creat personal viewing list</li>
                                <li>Rate and comment on films</li>
                            </ul>
                            <div class="text-center">
                                <a @click.prevent="subscribe('maker_yearly')" href="#" class="btn btn-warning"><span v-if="!loading">{{__('Sign me up!')}}</span><div v-if="loading" class="loading"></div></a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="card-details container-fluid" v-show="chosenPlan == 'watcher_monthly' || chosenPlan == 'watcher_yearly'">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="tarif-box">
                                <form :action="setAction()" @submit.prevent="makeToken()" method="post" id="payment-form" v-show="chosenPlan!='campaign'">
                                    {{ csrf_field() }}
                                    {{--<input type="hidden" name="stripeToken" v-model="token" />--}}
                                    <div class="form-row">
                                        <label for="card-element">
                                            Credit or debit card
                                        </label>
                                        <div id="card-element">
                                            <!-- a Stripe Element will be inserted here. -->
                                        </div>

                                        <!-- Used to display Element errors -->
                                        <div id="card-errors" role="alert"></div>
                                    </div>
                                    <br>
                                    <div class="text-center form-group tc-checkbox" v-if="chosenPlan === 'maker_monthly' || chosenPlan === 'maker_yearly'">
                                        <input type="checkbox" name="filmmaker-accept-tc" id="filmmaker-accept-tc" required @change="toggleTermsnConditions">
                                        <label for="filmmaker-accept-tc">I accept the <a href="/termsfm">Film maker Terms & Conditions</a></label>
                                    </div>
                                    <div class="text-center form-group tc-checkbox" v-if="chosenPlan === 'watcher_monthly' || chosenPlan === 'watcher_yearly'">
                                        <input type="checkbox" name="filmwatcher-accept-tc" id="filmwatcher-accept-tc" required @change="toggleTermsnConditions">
                                        <label for="filmwatcher-accept-tc">I accept the <a href="/termsfw">Film watcher Terms & Conditions</a></label>
                                    </div>
                                    <div class="text-center">
                                        <button class="btn btn-warning" v-if="tcAccepted"><span v-if="!loading">Subscribe</span><div v-if="loading" class="loading"></div></button>
                                    </div>
                                </form>
                            </div>
                            <br><br>
                            <div class="text-center" v-if="!showCampaignScreen && !receivedCode.code">
                                <a href="#" @click.prevent="campaign" class="orange">{{__('I have a campaign code and want to apply it')}}</a>
                            </div>
                            <div class="text-center" v-if="receivedCode.code">
                                {{--<span class="orange"> @{{ "Code " + receivedCode.code + " will give you additional " + receivedCode.free_days + " days"}}</span>--}}
                                <span class="orange"> @{{ "Code " + receivedCode.code + " accepted"}}</span>
                            </div>
                            <transition name="slide">
                                <div class="tarif-box" v-show="showCampaignScreen">
                                    <form action="#" @submit.prevent="applyCode" method="post" id="payment-form">
                                        {{ csrf_field() }}
                                        <input type="text" name="code" class="form-control" @keyUp="clearCode" v-model="code" />
                                        <span v-if="codeError" class="help-block">
                                        <strong v-text="codeError"></strong>
                                    </span>
                                        <br>
                                        <div class="text-center">
                                            <button class="btn btn-warning"><span v-if="!loadingCampaign">Redeem code</span><div v-if="loadingCampaign" class="loading"></div></button>
                                        </div>
                                    </form>
                                </div>
                            </transition>
                            <br>
                            <div class="text-center" v-if="!showCampaignScreen">
                                <a href="/" class="orange">{{__('Go Back')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </subscription-chooser>
@endsection
@section('js')
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        (function () {
  equalHeight(false)
})()

window.onresize = function () {
  equalHeight(true)
}

function equalHeight (resize) {
  var elements = document.getElementsByClassName('equalHeight'),
    allHeights = [],
    i = 0
  if (resize === true) {
    for (i = 0; i < elements.length; i++) {
      elements[i].style.height = 'auto'
    }
  }
  for (i = 0; i < elements.length; i++) {
    var elementHeight = elements[i].clientHeight
    allHeights.push(elementHeight)
  }
  for (i = 0; i < elements.length; i++) {
    elements[i].style.height = Math.max.apply(Math, allHeights) + 'px'
    // Optional: Add show class to prevent FOUC
    if (resize === false) {
      elements[i].className = elements[i].className + ' show';
    }
  }
}
    </script>
@endsection