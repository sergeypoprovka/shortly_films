@extends('layouts.error')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 style="font-size: 200px; font-family: 'Open Sans', sans-serif; font-weight: 100;">403</h1>
                <p>{{__('You are not allowed to be here')}}</p>
                <p><a href="/" class="btn btn-warning btn-lg">Go home</a> </p>
            </div>
        </div>
    </div>
@endsection