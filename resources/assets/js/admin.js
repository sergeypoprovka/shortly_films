import swal from 'sweetalert2'
(function ($) {
    $(function () {
        $('button.btn-danger').click(function (event) {
            var button = this;
            event.preventDefault();

            swal({
                title: 'Are you sure you want to delete this?',
                text: "You won't be able to revert this!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    jQuery(button).closest("form").submit();
                }
            })
        });
    });
})(jQuery);
