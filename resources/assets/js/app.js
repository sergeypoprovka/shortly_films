/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')
import 'core-js'
import Editor from '@tinymce/tinymce-vue'

window.Vue = require('vue')

import VueTransmit from 'vue-transmit'
import vSelect from 'vue-select'
import FileUpload from 'v-file-upload'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'))
Vue.component('video-player', require('./components/VideoPlayer.vue'))
Vue.component('subscription-chooser', require('./components/SubscriptionChooser.vue'))
Vue.component('video-like-later', require('./components/VideoLikeLater.vue'))
Vue.component('disqus', require('./components/Disqus.vue'))
Vue.component('video-upload', require('./components/VideoUpload.vue'))
Vue.component('taggable', require('./components/Taggable.vue'))
Vue.component('v-select', vSelect)
Vue.component('setting-groups', require('./components/SettingGroups.vue'))
Vue.component('setting-blocks', require('./components/SettingBlocks.vue'))

Vue.component('user-membership', require('./components/User/Membership'))
Vue.component('user-profile', require('./components/User/Profile'))
Vue.component('user-billing', require('./components/User/Billing'))
Vue.component('user-security', require('./components/User/Security'))
Vue.component('user-channel', require('./components/User/Channel'))
Vue.component('edit-video', require('./components/User/EditVideo'))
Vue.component('avatar', require('./components/User/Avatar'))
Vue.component('search', require('./components/Search'))
Vue.component('filter-films', require('./components/FilterFilms'))
Vue.component('time-interval-filter', require('./components/TimeIntervalFilter'))
Vue.component('rating', require('./components/Rating'))
Vue.component('subtitle-upload', require('./components/Video/SubtitleUpload'))
Vue.component('follow-user', require('./components/FollowUser'))
Vue.component('sidebar-follows', require('./components/SidebarFollows'))

Vue.component('feedback', require('./components/Feedback'))
Vue.component('registration', require('./components/User/Registration'))
Vue.component('login', require('./components/User/Login'))
Vue.component('auth-menu', require('./components/User/AuthMenu'))
Vue.component('tinymce', Editor)

$(function () {
  $('video')
    .bind('contextmenu', function (e) {
      e.preventDefault()
    })
  $('#preview-shortly').on('hidden.bs.modal', function () {
    $('#preview-shortly-video')[0].pause()
  })
})
Vue.use(FileUpload)
Vue.use(VueTransmit)

const EventBus = new Vue()

export default EventBus

const app = new Vue({el: '#app'})
