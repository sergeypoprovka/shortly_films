<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Laravel\Cashier\Cashier;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Cashier::useCurrency('sek', 'kr');

        View::composer('*', function ($view) {
            if(Auth::check()) {
                $user_account = User::where('id','=',Auth::user()->id)->with('profile')->first();
                $view->with('user_account', $user_account);
                $view->with('subscription', $user_account->subscriptions);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
