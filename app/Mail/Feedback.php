<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Feedback extends Mailable
{
    use Queueable, SerializesModels;

    private $params;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_string, $message_string)
    {
        $this->params['email'] = $email_string;
        $this->params['message'] = $message_string;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.feedback')->with(['email'=>$this->params['email'], 'message_string'=>$this->params['message']]);
    }
}
