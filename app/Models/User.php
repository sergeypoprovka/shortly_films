<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable;
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'role','stripe_id','card_brand','card_last_four','trial_ends_at','subscription_period_start','subscription_period_end'
    ];

    protected $dates = ['trial_ends_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile(){
        return $this->hasOne(Profile::class,'user_id','id');
    }

    public function videos(){
        return $this->hasMany(Video::class,'user_id','id');
    }

    public function follows(){
    	return $this->hasMany(Follow::class,'user_id','id');
    }

    public function followers(){
	    return $this->hasMany(Follow::class,'follow_id','id');
    }
}
