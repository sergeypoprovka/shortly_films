<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name','parent','slug'];

    function videos(){
        return $this->belongsToMany(Video::class, 'video_categories');
    }
}
