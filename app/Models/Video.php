<?php

namespace App\Models;

use App\Models\VideoThumbnails;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Video extends Model
{
    protected $fillable = [
        'title',
        'script',
        'user_id',
        'originalName',
        'originalExtension',
        'aws_original_link',
        'aws_prefix_folder',
        'original_title',
        'country',
        'language',
        'producer',
        'director',
        'actors',
        'awards',
        'production_year',
        'release_year',
        'soundtrack',
        'videoDuration',
        'method_of',
        'storyline',
        'didyouknow',
        'script',
        'photo',
        'approved'
        ];

    function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    function tags(){
        return $this->belongsToMany(Tag::class, 'video_tags' );
    }

    function cats(){
        return $this->belongsToMany(Category::class,'video_categories');
    }

    function thumbs(){
        return $this->hasMany(VideoThumbnails::class, 'video_id','id');
    }

    function keywords(){
        return $this->belongsToMany(Keyword::class, 'video_keywords');
    }

    function restrictions(){
        return $this->belongsToMany(VideoRestriction::class, 'video_restrictions');
    }

    public function styles(){
        return $this->hasOne(MethodOf::class, 'id','method_of');
    }

    public function languages(){
        return $this->hasOne(Language::class, 'id','language');
    }

    public function countries(){
        return $this->hasOne(Country::class, 'id','country');
    }

    function videotimer(){
        return $this->hasOne(VideoTimer::class, 'video_id','id')->orderBy('id','DESC');
    }

    function watched(){
        return $this->hasMany(VideoView::class,'video_id','id')->sum('time_watched');
    }

    function likedByUser(){
        return Like::where([['user_id','=',Auth::user()->id],['video_id','=',$this->id]])->count();
    }

    function addedToWatchLaterByUser(){
        return WatchLater::where([['user_id','=',Auth::user()->id],['video_id','=',$this->id]])->count();
    }

    public function subtitles(){
        return $this->hasMany(Subtitle::class,'video_id','id');
    }
}
