<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Video;
use App\Models\Subtitle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubtitleController extends Controller
{
    public function getSubtitles(Video $video){
        return Subtitle::where('video_id','=',$video->id)->with('language')->get();
    }

    public function uploadSubtitles(Request $request, Video $video){
        $request->validate([
            'lang_id'=>'required|integer'
        ]);
        $file = $request->file('file');
        $lang = Language::find($request->lang_id);
        $path = $file->storeAs('/uploads/subtitles/'.$video->id.'/',$lang->iso_code.".".$file->getClientOriginalExtension());
        if($path) {
            $subs = Subtitle::create([
                'video_id'=>$request->video->id,
                'language' => $request->lang_id,
                'file' => $lang->iso_code.".".$file->getClientOriginalExtension()
            ]);

            if($subs){
                return $subs->id;
            }
        }
    }

    public function removeSubtitles(Subtitle $subtitle){
        if(Subtitle::destroy($subtitle->id)){
            return "1";
        }
        return "0";
    }
}
