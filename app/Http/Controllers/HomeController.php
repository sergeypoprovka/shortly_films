<?php

namespace App\Http\Controllers;

use App\Mail\Feedback;
use App\Models\Category;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Video::select('*');
        if(request()->time){
            if(request()->time == '1'){
                $query->where('videoDuration','<=','00:01:00.000');
            }elseif (request()->time == '45plus'){
                $query->where('videoDuration','>','00:46:00.000');
            }else{
                $diff = explode("_",request()->time);
                $query->where([['videoDuration','=>','00:'.sprintf("%02d", $diff[0]).':00.000'],['videoDuration','<=','00:'.sprintf("%02d", $diff[1]).':00.000']]);
            }

        }
        if(request()->range){
            switch(request()->range){
                case "newest":
                    $query->orderBy('id','DESC');
                    break;
                case "oldest":
                    $query->orderBy('id','ASC');
                    break;
            }
        }else{
            $query->orderBy('id','DESC');
        }
        if(request()->genre){
            $genre = request()->genre;
            $query->whereHas('cats', function ($query) use ($genre){
                $query->where([['categories.id', '=', request()->genre]]);
            });
        }
        $videos = $query->where([['approved','=','1']])->paginate(20);
        $genres = Category::all();
        return view('home', compact('videos','genres'));
    }


    function gets3Path($value)
    {
        $disk = Storage::disk('s3');

        if ($disk->exists($value)) {
            $command = $disk->getDriver()->getAdapter()->getClient()->getCommand('GetObject', [
                'Bucket' => env('AWS_BUCKET'),
                'Key' => $value,
            ]);

            $request = $disk->getDriver()->getAdapter()->getClient()->createPresignedRequest($command, '+30 seconds');

            return (string) $request->getUri();
        }

        return $value;
    }

    function sendFeedback(Request $request){
        $request->validate([
            'email'=>'required|email',
            'message'=>'required|string'
        ]);

        Mail::to(env('FEEDBACK_EMAIL'))->send(new Feedback($request->input('email'), $request->input('message')));

        return 1;
    }
}
