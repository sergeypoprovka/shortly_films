<?php

namespace App\Http\Controllers;

use App\Models\Video;
use App\Models\VideoView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VideoViewController extends Controller
{
    public function collectTimeframes(Request $request, Video $video){
        $watched_time = 0;
        $timeframes = $request->timeframes;
        foreach($timeframes as $timeframe){
            $watched_time += $timeframe['stop'] - $timeframe['start'];
        }
        $tf = VideoView::create([
            'user_id'=>Auth::user()->id,
            'video_id'=>$request->video->id,
            'time_watched'=>round($watched_time,2,PHP_ROUND_HALF_DOWN)
        ]);
        if($tf){
            return 1;
        }
    }
}
