<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeUserEmail;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Spatie\Newsletter\Newsletter;

class AuthController extends Controller
{
    public function login(Request $request){
        $request->validate([
            'email'=>'required|email',
            'password'=>'required|string'
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            return 1;
        }
    }

    public function register(Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = User::create([
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role'=> $request->role
        ]);

        if($user){
            $profile = Profile::create([
                'user_id'=>$user->id,
                'name'=>$request->name,
                'lastname'=>$request->lastname
            ]);

            \Newsletter::subscribe($user->email, [ 'FNAME'=>(string)$request->name, 'LNAME'=>(string)$request->lastname ]);

            if($profile){
                Mail::to($user->email)->send(new WelcomeUserEmail($user));
                return $this->login($request);
            }
        }
    }
}
