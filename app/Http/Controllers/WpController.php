<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeUserEmail;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Spatie\Newsletter\Newsletter;

class WpController extends Controller
{
    public function login(Request $request){
        $request->validate([
            'email'=>'required|email',
            'password'=>'required'
        ]);

        Auth::attempt(['email'=>$request->email,'password'=>$request->password]);
        if($user = Auth::user()){
            return $user;
        }else{
            return response()->json(['error'=>'403','message'=>'Fuck off!']);
        }
    }

    public function register(Request $request){
        $request->validate([
            'email'=>'required|email|unique:users',
            'password'=>'required|confirmed',
            'name'=>'required|string',
            'lastname'=>'required|string',
            'role'=>'required|string'
        ]);

        $user = User::create([
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role'=> $request->role=='maker_monthly' ? 'Filmmaker' : 'User'
        ]);

        if($user){
            $profile = Profile::create([
                'user_id'=>$user->id,
                'name'=>$request->name,
                'lastname'=>$request->lastname
            ]);

            \Newsletter::subscribe($user->email, [ 'FNAME'=>(string)$request->name, 'LNAME'=>(string)$request->lastname ]);

            if($profile){
                Mail::to($user->email)->send(new WelcomeUserEmail($user));
                Auth::attempt(['email'=>$request->email,'password'=>$request->password]);
                if($user = Auth::user()){
                    return $user;
                }else{
                    return response()->json(['error'=>'403','message'=>'Fuck off!']);
                }
            }
        }
    }
}
