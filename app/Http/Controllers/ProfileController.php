<?php

namespace App\Http\Controllers;

use App\Models\Follow;
use App\Models\User;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

	function showProfile(User $user){
		$videos = $user->videos()->paginate(20);
		return view('profile.show', compact('videos', 'user'));
	}

	function follow(Request $request){
		$request->validate([
			'follow_user'=>'required|integer'
		]);

		$follow = Follow::firstOrCreate([
			'user_id'=>Auth::user()->id,
			'follow_id'=>$request->follow_user
		]);

		return [
			"id"=>$follow->follower->profile->id,
			"name"=>$follow->follower->profile->name,
			"lastname"=>$follow->follower->profile->lastname
		];
	}

    function unfollow(Follow $follow){
	    if(Follow::destroy($follow->id)){
	        return redirect()->back();
        }
    }

	function getFollows(User $user){
	    $conditions = [
	        ['user_id','=',Auth::user()->id]
        ];
        if($user->id){
            $conditions[] = ['follow_id','=',$user->id];
        }
		$follows = Follow::where($conditions)->orderBy('id','DESC')->limit(20)->get();
		$list = [];
		foreach($follows as $follow){
			$list[] = [
				'id'=>$follow->follower->id,
				'name'=>$follow->follower->profile->name,
				'lastname'=>$follow->follower->profile->lastname,
				'image'=>$follow->follower->profile->image
			];
		}
		return $list;
	}

	function allFollows(){
	    $follows = Follow::where('user_id','=',Auth::user()->id)->paginate(20);
	    return view('profile.follows', compact('follows'));
    }
}
