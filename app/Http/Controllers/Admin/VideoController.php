<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Tag;
use App\Models\Video;
use App\Models\VideoCategory;
use App\Models\VideoTag;
use App\Models\VideoThumbnails;
use Aws\ElasticTranscoder\ElasticTranscoderClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    public function index(){
        $videos = Video::paginate(20);

        return view('admin.videos.index', compact('videos'));
    }

    public function create(){
        return view('admin.videos.create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'title'=>'required|string',
            'description'=>'string|nullable',
            'video'=>'required'
        ]);

        if($vUpload = $request->video->store('videos','s3')){

            $originalName = $request->video->getClientOriginalName();
            $originalExtension = $request->video->getClientOriginalExtension();

            $elasticTranscoder = ElasticTranscoderClient::factory(array(
                'credentials' => array(
                    'key' => env("AWS_KEY"),
                    'secret' => env("AWS_SECRET"),
                ),
                'region' => 'eu-west-1',
                'version' => '2012-09-25'
            ));

            $job = $elasticTranscoder->createJob(array(

                'PipelineId' => '1502896980884-7mgf88',

                'OutputKeyPrefix' => 'videos/'.md5(time().$originalName).'/',

                'Input' => array(
                    'Key' => $vUpload,
                    'FrameRate' => 'auto',
                    'Resolution' => 'auto',
                    'AspectRatio' => 'auto',
                    'Interlaced' => 'auto',
                    'Container' => 'auto',
                ),

                'Outputs' => array(
                    array(
                        'Key' => '360p_16-9__'.$originalName,
                        'Rotate' => 'auto',
                        'PresetId' => '1351620000001-000040',
                        "ThumbnailPattern" => "360p_16-9__-{count}"
                    ),
                    array(
                        'Key' => '480p_16-9__'.$originalName,
                        'Rotate' => 'auto',
                        'PresetId' => '1351620000001-000020',
                        "ThumbnailPattern" => "480p_16-9__-{count}"
                    ),
                    array(
                        'Key' => '720p_16-9__'.$originalName,
                        'Rotate' => 'auto',
                        'PresetId' => '1351620000001-000010',
                        "ThumbnailPattern" => "720p_16-9__-{count}"
                    ),
                    array(
                        'Key' => '1080p_16-9__'.$originalName,
                        'Rotate' => 'auto',
                        'PresetId' => '1351620000001-000001',
                        "ThumbnailPattern" => "1080p_16-9__-{count}"
                    ),
                    array(
                        'Key' => 'hls2m__'.$originalName,
                        'Rotate' => 'auto',
                        'PresetId' => '1351620000001-200010',
                        "ThumbnailPattern" => "hls2m__-{count}",
                        'SegmentDuration'=>'15'
                    ),

                ),
            ));

            $jobData = $job->get('Job');

            if($jobData['OutputKeyPrefix']){
                $video = Video::create([
                    'title'=>$request->title,
                    'description'=>$request->description,
                    'user_id'=>Auth::user()->id,
                    'originalName'=>$originalName,
                    'originalExtension'=>$originalExtension,
                    'aws_original_link'=>$jobData['Input']['Key'],
                    'aws_prefix_folder'=>$jobData['OutputKeyPrefix']
                ]);

                $this->insertToCategories($request->categories, $video->id);
                $this->insertToTags($request->tags, $video->id);

                if($video){
                    \Session::flash('success',__('Video uploaded successfully'));

                    return redirect('/admin/videos');
                }
            }
        }
    }



    public function approve(Request $request, Video $video){
        $video->approved = 1;
        if($video->save()){
            \Session::flash('success','Video approved');
        }
        return redirect()->back();
    }

    public function unapprove(Request $request, Video $video){
        $video->approved = 0;
        if($video->save()){
            \Session::flash('success','Video denied');
        }
        return redirect()->back();
    }

    public function show(Video $video){
        $links = [];

        if(Storage::disk('s3')->exists($video->aws_original_link)) {$links['original'] = Storage::disk('s3')->url($video->aws_original_link);}
        if(Storage::disk('s3out')->exists($video->aws_prefix_folder."1080p_16-9__".$video->originalName)){ $links['1080p']= Storage::disk('s3out')->url($video->aws_prefix_folder."1080p_16-9__".$video->originalName);}
        if(Storage::disk('s3out')->exists($video->aws_prefix_folder."720p_16-9__".$video->originalName)){ $links['720p']= Storage::disk('s3out')->url($video->aws_prefix_folder."720p_16-9__".$video->originalName);}
        if(Storage::disk('s3out')->exists($video->aws_prefix_folder."480p_16-9__".$video->originalName)){ $links['480p']= Storage::disk('s3out')->url($video->aws_prefix_folder."480p_16-9__".$video->originalName);}
        if(Storage::disk('s3out')->exists($video->aws_prefix_folder."360p_16-9__".$video->originalName)){ $links['360p']= Storage::disk('s3out')->url($video->aws_prefix_folder."360p_16-9__".$video->originalName);}

        if(Storage::disk('s3thumbs')->exists($video->aws_prefix_folder."360p_16-9__-00001.png")){
            $links['thumbs'][] = Storage::disk('s3thumbs')->url($video->aws_prefix_folder."360p_16-9__-00001.png");
        }
        if(Storage::disk('s3thumbs')->exists($video->aws_prefix_folder."360p_16-9__-00002.png")){ $links['thumbs'][] = Storage::disk('s3thumbs')->url($video->aws_prefix_folder."360p_16-9__-00002.png");}
        if(Storage::disk('s3thumbs')->exists($video->aws_prefix_folder."360p_16-9__-00003.png")){ $links['thumbs'][] = Storage::disk('s3thumbs')->url($video->aws_prefix_folder."360p_16-9__-00003.png");}

        return view('admin.videos.view', compact('video','links'));
    }

    public function destroy(Video $video){
        if(Video::destroy($video->id)){
            Storage::disk('s3')->delete($video->aws_original_link);
            Storage::disk('s3out')->deleteDirectory($video->aws_prefix_folder);
            Storage::disk('s3thumbs')->deleteDirectory($video->aws_prefix_folder);

            \Session::flash('success',__('Files deleted'));
        }

        return redirect('/admin/videos');
    }

    function gets3Path($value, $bucket)
    {
        $disk = Storage::disk('s3');

        if ($disk->exists($value)) {
            $command = $disk->getDriver()->getAdapter()->getClient()->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key' => $value,
            ]);

            $request = $disk->getDriver()->getAdapter()->getClient()->createPresignedRequest($command, '+30 minutes');

            return (string) $request->getUri();
        }

        return $value;
    }

    private function insertToCategories($cat_list, $video_id){
        $ex_cat = explode(',',$cat_list);
        foreach($ex_cat as $cat){
            $cat_id = Category::where(['name'=>$cat])->first();
//            echo "Cat:".$cat_id->id;
            VideoCategory::create([
                'category_id'=>$cat_id->id,
                'video_id'=>$video_id
            ]);
        }
    }

    private function insertToTags($tag_list, $video_id){
        $ex_tag = explode(',',$tag_list);
        foreach($ex_tag as $tag){
            $tag_id = Tag::where(['name'=>$tag])->first();
//            echo "Tags:".$tag_id->id;
            VideoTag::create([
                'tag_id'=>$tag_id->id,
                'video_id'=>$video_id
            ]);
        }
    }

    public function setMainThumb(Request $request){
        $request->validate([
            'url'=>'required|url',
            'vid'=>'required|integer'
        ]);
        $video = Video::find($request->vid);
        $video->main_thumb_url = $request->url;
        if($video->save()){
            return 1;
        }
    }

    public function uploadNewThumb(Request $request, Video $video){
        $file = $request->file('image');
        $filename = md5($file->getClientOriginalName().time()).".".$file->getClientOriginalExtension();
        $request->image->move(public_path('/thumbnails'), $filename);
        $url = env('APP_URL').'/thumbnails/'.$filename;

        VideoThumbnails::create([
            'video_id'=>$video->id,
            'image_url'=>$url
        ]);

        return $url;
    }

    public function getAdditionalThumbs(Video $video){
        return $video->thumbs;
    }
}
