<?php

namespace App\Http\Controllers\Admin;

use App\Models\Code;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $codes = Code::paginate(20);

        return view('admin.codes.index', compact('codes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.codes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code'=>'required|string',
            'for_role'=>'required|string',
            'allow_multiple'=>'required|integer',
            'free_days'=>'required|integer|max:1000'
        ]);

        $code = Code::create([
            'code'=>$request->code,
            'for_role'=>$request->for_role,
            'free_days'=>$request->free_days,
            'allow_multiple'=>$request->allow_multiple,
            'used'=>0
        ]);

        if($code){
            \Session::flash('success','Code created');
        }

        return redirect('/admin/codes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function show(Code $code)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function edit(Code $code)
    {
        return view('admin.codes.edit', compact('code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Code $code)
    {
        $request->validate([
            'code'=>'required|string',
            'for_role'=>'required|string',
            'allow_multiple'=>'required|integer',
            'free_days'=>'required|integer|max:1000'
        ]);

        $code->code = $request->code;
        $code->for_role = $request->for_role;
        $code->allow_multiple = $request->allow_multiple;
        $code->free_days = $request->free_days;

        if($code->save()){
            \Session::flash('success','Code updated');
        }

        return redirect('/admin/codes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Code  $code
     * @return \Illuminate\Http\Response
     */
    public function destroy(Code $code)
    {
        if(Code::destroy($code->id)){
            \Session::flash('danger','Code deleted');
        }
        return redirect()->back();
    }
}
