<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Video;
use App\Models\VideoView;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function byUser(){
        $users = User::select(DB::raw("users.id, users.email, profiles.name, profiles.lastname, users.role, SUM(video_views.time_watched) as sum"))
            ->join('profiles','profiles.user_id','=','users.id')
            ->join('videos','videos.user_id','=','users.id')
            ->join('video_views','videos.id','=','video_views.video_id')
            ->groupBy('users.email','profiles.name','profiles.lastname')
            ->paginate(20);
        return view('admin.reports.user', compact('users'));
    }

    public function byVideo(){
        $videos = VideoView::select(DB::raw('video_id, SUM(time_watched) as sum'))->groupBy('video_id')->paginate(20);
        return view('admin.reports.video',compact('videos'));
    }

    public function byVideoDetailed(Video $video){
        $videos = VideoView::where('video_id','=',$video->id)->paginate(20);
        return view('admin.reports.video_detailed',compact('videos'));
    }
}
