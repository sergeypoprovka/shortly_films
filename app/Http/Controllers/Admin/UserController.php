<?php

namespace App\Http\Controllers\Admin;

use App\Models\Profile;
use App\Models\User;
use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(20);
        $countusers = User::count();
        $countadmins = User::where('role','=','Administrator')->count();
        $countfws = User::where('role','=','User')->count();
        $countfws_payed = User::where([['role','=','User'],['card_last_four','!=',NULL]])->count();
        $countfms = User::where('role','=','Filmmaker')->count();;
        $countfms_payed = User::where([['role','=','Filmmaker'],['card_last_four','!=',NULL]])->count();;

        return view('admin.users.index', compact('users','countadmins','countfms','countfms_payed','countfws','countfws_payed','countusers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|string',
            'lastname'=>'required|string',
            'role'=>'required',
            'email'=>'email|required|unique:users',
            'password'=>'required|confirmed'
        ]);

        $user = User::create([
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'role'=>$request->role
        ]);

        if($user){
            $profile = Profile::create([
                'user_id'=>$user->id,
                'name'=>$request->name,
                'lastname'=>$request->lastname
            ]);
            if($profile) {
                \Session::flash('success', __('User created'));
            }
        }

        return redirect('/admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('admin.users.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        // TODO: Check unique email on update
        $this->validate($request, [
            'name'=>'required|string',
            'lastname'=>'required|string',
            'role'=>'required',
            'email'=>'email|required',
        ]);

        $user->email = $request->email;
        $user->role = $request->role;
        $user->profile->name = $request->name;
        $user->profile->lastname = $request->lastname;

        if($user->save() && $user->profile->save()){
            \Session::flash('success',__('User updated'));
        }

        return redirect('/admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy(User $user)
    {
//        TODO: Stupid but fast solution - need to refactor to onDelete('cascade')
        $videos = Video::where([['user_id','=',$user->id],['approved','=','1']])->get();
        if(count($videos) > 0){
            foreach($videos as $video){
                Video::destroy($video->id);
            }
        }
        if(User::destroy($user->id)){
            \Session::flash('success','User deleted');
        }

        return redirect('/admin/users');
    }
}
