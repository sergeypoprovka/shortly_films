<?php

namespace App\Http\Controllers;

use App\Models\StaticPage;
use App\Models\Subtitle;
use App\Models\Video;
use App\Models\VideoRestriction;
use Illuminate\Http\Request;
use Aws\ElasticTranscoder\ElasticTranscoderClient;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

class VideoController extends Controller
{

    public function upload(){
        $this->authorize('upload', Video::class);
        return view('video.upload');
    }

    public function storevideo(Request $request){

        $this->validate($request, [
            'title'=>'required|string',
            'original_title'=>'required|string',
            'country'=>'required|integer',
            'language'=>'required|integer',
            'producer'=>'required',
            'director'=>'required',
            'script'=>'string|nullable',
            'photo'=>'nullable',
            'actors'=>'nullable',
            'awards'=>'nullable',
            'production_year'=>'required|integer',
            'release_year'=>'required|integer',
            'storyline'=>'nullable',
            'keywords'=>"nullable|array",
            'tags'=>"nullable|array",
            'genres'=>'required',
            'soundtrack'=>'nullable',
            'didyouknow'=>'nullable'
        ]);

//        if($vUpload = $request->video->store('videos','s3')){
//
//            $originalName = $request->video->getClientOriginalName();
//            $originalExtension = $request->video->getClientOriginalExtension();
//
//            $elasticTranscoder = ElasticTranscoderClient::factory(array(
//                'credentials' => array(
//                    'key' => env("AWS_KEY"),
//                    'secret' => env("AWS_SECRET"),
//                ),
//                'region' => 'eu-west-1',
//                'version' => '2012-09-25'
//            ));
//
//            $job = $elasticTranscoder->createJob(array(
//
//                'PipelineId' => '1502896980884-7mgf88',
//
//                'OutputKeyPrefix' => 'videos/'.md5(time().$originalName).'/',
//
//                'Input' => array(
//                    'Key' => $vUpload,
//                    'FrameRate' => 'auto',
//                    'Resolution' => 'auto',
//                    'AspectRatio' => 'auto',
//                    'Interlaced' => 'auto',
//                    'Container' => 'auto',
//                ),
//
//                'Outputs' => array(
//                    array(
//                        'Key' => '360p_16-9__'.$originalName,
//                        'Rotate' => 'auto',
//                        'PresetId' => '1351620000001-000040',
//                        "ThumbnailPattern" => "360p_16-9__-{count}"
//                    ),
//                    array(
//                        'Key' => '480p_16-9__'.$originalName,
//                        'Rotate' => 'auto',
//                        'PresetId' => '1351620000001-000020',
//                        "ThumbnailPattern" => "480p_16-9__-{count}"
//                    ),
//                    array(
//                        'Key' => '720p_16-9__'.$originalName,
//                        'Rotate' => 'auto',
//                        'PresetId' => '1351620000001-000010',
//                        "ThumbnailPattern" => "720p_16-9__-{count}"
//                    ),
//                    array(
//                        'Key' => '1080p_16-9__'.$originalName,
//                        'Rotate' => 'auto',
//                        'PresetId' => '1351620000001-000001',
//                        "ThumbnailPattern" => "1080p_16-9__-{count}"
//                    ),
//                    array(
//                        'Key' => 'hls2m__'.$originalName,
//                        'Rotate' => 'auto',
//                        'PresetId' => '1351620000001-200010',
//                        "ThumbnailPattern" => "hls2m__-{count}",
//                        'SegmentDuration'=>'15'
//                    ),
//
//                ),
//            ));
//
//            $jobData = $job->get('Job');
//
//            if($jobData['OutputKeyPrefix']){
                $video = Video::create([
                    'title'=>$request->title,
                    'script'=>$request->script,
                    'user_id'=>Auth::user()->id,
                    'videoDuration'=>$request['videoUploadResult']['duration'],
                    'originalName'=>$request['videoUploadResult']['originalName'],
                    'originalExtension'=>$request['videoUploadResult']['originalExtension'],
                    'aws_original_link'=>$request['videoUploadResult']['jobData']['Input']['Key'],
                    'aws_prefix_folder'=>$request['videoUploadResult']['jobData']['OutputKeyPrefix'],
                    'original_title'=>$request->original_title,
                    'country'=>$request->country,
                    'language'=>$request->language,
                    'producer'=>$request->producer,
                    'director'=>$request->director,
                    'actors'=>$request->actors,
                    'awards'=>$request->awards,
                    'method_of'=>$request->method_of,
                    'production_year'=>$request->production_year,
                    'release_year'=>$request->release_year,
                    'storyline'=>$request->storyline,
                    'keywords'=>$request->keywords,
                    'soundtrack'=>$request->soundtrack,
                    'didyouknow'=>$request->didyouknow,
                    'photo'=>$request->photo
                ]);

                if($video){
                    if(count($request->tags) > 0) {
                        foreach($request->tags as $tag)
                        $video->tags()->attach($tag);
                    }
                    if(count($request->keywords) > 0) {
                        foreach($request->keywords as $keyword)
                            $video->keywords()->attach($keyword);
                    }
                    if(count($request->genres) > 0) {
                        foreach($request->genres as $genre)
                            $video->cats()->attach($genre);
                    }
                    if(count($request->restrictedCountries) > 0) {
                        foreach($request->restrictedCountries as $restrictedCountry)
                            $video->restrictions()->attach($restrictedCountry);
                    }

                    if(is_dir(storage_path('app/uploads/temp_subtitles/'.Auth::user()->id))){
                        $temp = storage_path('app/uploads/temp_subtitles/'.Auth::user()->id);
                        rename($temp, storage_path('app/uploads/subtitles/'.$video->id));
                    }

                    if($request->subtitleIds){
                        foreach($request->subtitleIds as $sId){
                            $sub = Subtitle::find($sId);
                            $sub->video_id = $video->id;
                            $sub->save();
                        }
                    }

                    return $video->id;
                }
    }

    public function storeVideoOnly(FileReceiver $receiver) {
        // check if the upload is success
        if ($receiver->isUploaded()) {
            // receive the file
            $save = $receiver->receive();
            // check if the upload has finished (in chunk mode it will send smaller files)
            if ($save->isFinished()) {
                // save the file and return any response you need
                return $this->uploadFile($save->getFile());
            } else {
                // we are in chunk mode, lets send the current progress
                /** @var AbstractHandler $handler */
                $handler = $save->handler();
                return response()->json([
                    "done" => $handler->getPercentageDone(),
                ]);
            }
        } else {
            throw new UploadMissingFileException();
        }
    }

    public function uploadFile(UploadedFile $file){
        $vUpload = $file->store('videos','s3');

        $originalName = $file->getClientOriginalName();
        $originalExtension = $file->getClientOriginalExtension();

        $elasticTranscoder = ElasticTranscoderClient::factory(array(
            'credentials' => array(
                'key' => env("AWS_KEY"),
                'secret' => env("AWS_SECRET"),
            ),
            'region' => 'eu-west-1',
            'version' => '2012-09-25'
        ));

        $job = $elasticTranscoder->createJob(array(

            'PipelineId' => '1502896980884-7mgf88',

            'OutputKeyPrefix' => 'videos/'.md5(time().$originalName).'/',

            'Input' => array(
                'Key' => $vUpload,
                'FrameRate' => 'auto',
                'Resolution' => 'auto',
                'AspectRatio' => 'auto',
                'Interlaced' => 'auto',
                'Container' => 'auto',
            ),

            'Outputs' => array(
                array(
                    'Key' => '360p_16-9__'.$originalName,
                    'Rotate' => 'auto',
                    'PresetId' => '1351620000001-000040',
                    "ThumbnailPattern" => "360p_16-9__-{count}"
                ),
                array(
                    'Key' => '480p_16-9__'.$originalName,
                    'Rotate' => 'auto',
                    'PresetId' => '1351620000001-000020',
                    "ThumbnailPattern" => "480p_16-9__-{count}"
                ),
                array(
                    'Key' => '720p_16-9__'.$originalName,
                    'Rotate' => 'auto',
                    'PresetId' => '1351620000001-000010',
                    "ThumbnailPattern" => "720p_16-9__-{count}"
                ),
                array(
                    'Key' => '1080p_16-9__'.$originalName,
                    'Rotate' => 'auto',
                    'PresetId' => '1351620000001-000001',
                    "ThumbnailPattern" => "1080p_16-9__-{count}"
                ),
                array(
                    'Key' => 'hls2m__'.$originalName,
                    'Rotate' => 'auto',
                    'PresetId' => '1351620000001-200010',
                    "ThumbnailPattern" => "hls2m__-{count}",
                    'SegmentDuration'=>'15'
                ),

            ),
        ));

        $jobData = $job->get('Job');

        if($jobData['OutputKeyPrefix']){
            return [
                'jobData'=>$jobData,
                'duration'=>$this->getDuration($file->getRealPath()),
                'originalName'=>$originalName,
                'originalExtension'=>$originalExtension
            ];
        }

        return false;
    }

    public function validate_step_2(Request $request){
        $v = $request->validate([
            'title'=>"required|string",
            'original_title'=>"required|string",
            'country'=>"required|integer",
            'language'=>"required|integer",
            'producer'=>"required|string",
            'director'=>"required|string",
            'actors'=>'nullable|string',
            'awards'=>"nullable|string",
            'soundtrack'=>"nullable|string",
            'production_year'=>"required|integer|min:1900",
            'release_year'=>"required|integer|min:1900"
        ]);
        if ($v) {
            return 1;
        } else {
            return $v;
        }
    }

    public function validate_step_3(Request $request){
        $v = $request->validate([
            'genres'=>"required|array|min:1",
            'method_of'=>'nullable|integer'
        ]);
        if ($v) {
            return 1;
        } else {
            return $v;
        }
    }

    public function validate_step_4(Request $request){
        $v = $request->validate([
            'keywords'=>"array|nullable",
            'tags'=>"array|nullable",
        ]);
        if ($v) {
            return 1;
        } else {
            return $v;
        }
    }

    public function show(Video $video){
        $restricted = false;
        $restrictedCountries = [];
        $userLocation = geoip()->getLocation();

        $restrictions = VideoRestriction::where('video_id','=',$video->id)->get();
        foreach($restrictions as $restriction){
            $restrictedCountries[] = $restriction->country->code;
        };


        if(in_array($userLocation->iso_code, $restrictedCountries)){
            $restricted = true;
        }

        $links = [];

        // GET ALL LINKS AND THUMBS

        if(Storage::disk('s3')->url($video->aws_original_link)) {$links['original'] = Storage::disk('s3')->url($video->aws_original_link);}
        if(Storage::disk('s3out')->url($video->aws_prefix_folder."1080p_16-9__".$video->originalName)){ $links['1080p']= Storage::disk('s3out')->url($video->aws_prefix_folder."1080p_16-9__".$video->originalName);}
        if(Storage::disk('s3out')->url($video->aws_prefix_folder."720p_16-9__".$video->originalName)){ $links['720p']= Storage::disk('s3out')->url($video->aws_prefix_folder."720p_16-9__".$video->originalName);}
        if(Storage::disk('s3out')->url($video->aws_prefix_folder."480p_16-9__".$video->originalName)){ $links['480p']= Storage::disk('s3out')->url($video->aws_prefix_folder."480p_16-9__".$video->originalName);}
        if(Storage::disk('s3out')->url($video->aws_prefix_folder."360p_16-9__".$video->originalName)){ $links['360p']= Storage::disk('s3out')->url($video->aws_prefix_folder."360p_16-9__".$video->originalName);}

        if(Storage::disk('s3thumbs')->url($video->aws_prefix_folder."360p_16-9__-00001.png")){ $links['thumbs'][] = Storage::disk('s3thumbs')->url($video->aws_prefix_folder."360p_16-9__-00001.png");}
        if(Storage::disk('s3thumbs')->url($video->aws_prefix_folder."360p_16-9__-00002.png")){ $links['thumbs'][] = Storage::disk('s3thumbs')->url($video->aws_prefix_folder."360p_16-9__-00002.png");}
        if(Storage::disk('s3thumbs')->url($video->aws_prefix_folder."360p_16-9__-00003.png")){ $links['thumbs'][] = Storage::disk('s3thumbs')->url($video->aws_prefix_folder."360p_16-9__-00003.png");}

        return view('video.view', compact('video','links', 'restricted'));
    }

    public function search(Request $request){
        $q = $request->q;
        $results = [];
        if($q != ""){
            $results = Video::where([['title','LIKE','%'.$q.'%'],['approved','=','1']])->paginate(20);
        }
        return view('video.search', compact('q','results'));
    }


    public function getDuration($full_video_path)
    {
        $getID3 = new \getID3;
        $file = $getID3->analyze($full_video_path);
        $playtime_seconds = $file['playtime_seconds'];
        $duration = date('H:i:s.v', $playtime_seconds);

        return $duration;
    }

    public function uploadVideoImage(Request $request){
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = Auth::user()->id."__".time().'.'.$image->getClientOriginalExtension();
            $destinationPath = storage_path('/app/uploads/videos/');
            $image->move($destinationPath, $name);

            return $name;
        }
    }

    public function searchAutocomplete(Request $request){
        return Video::where([['title','LIKE','%'.$request->q.'%'],['approved','=','1']])->limit(5)->get();
    }

    public function getHelp(){
        return StaticPage::where([['slug','=','upload_form_help']])->first();
    }

    public function edit(Video $video){
        $restrictions = VideoRestriction::where('video_id','=',$video->id)->with('country')->get();
        return ['video'=>Video::with('cats','keywords','styles','tags','languages','countries')->find($video->id),'restrictions'=>$restrictions];
    }

    public function update(Request $request, Video $video){
        $request->validate([
            'title'=>'required|string',
            'original_title'=>'required|string',
            'country'=>'required|integer',
            'language'=>'required|integer',
            'producer'=>'required',
            'director'=>'required',
            'script'=>'string|nullable',
            'photo'=>'nullable',
            'actors'=>'nullable',
            'awards'=>'nullable',
            'production_year'=>'required|integer|min:1900',
            'release_year'=>'required|integer|min:1900',
            'storyline'=>'nullable',
            'keywords'=>"nullable|array",
            'tags'=>"nullable|array",
            'genres'=>'required',
            'soundtrack'=>'nullable',
            'didyouknow'=>'nullable',
            'genres'=>"required|array|min:1",
            'method_of'=>'nullable|integer',
            'keywords'=>"array|nullable",
            'tags'=>"array|nullable",
        ]);

        $video->user_id = Auth::user()->id;
        $video->title = $request->title;
        $video->original_title = $request->original_title;
        $video->country = $request->country;
        $video->language = $request->language;
        $video->producer =$request->producer;
        $video->director =$request->director;
        $video->actors =$request->actors;
        $video->awards =$request->awards;
        $video->soundtrack =$request->soundtrack;
        $video->production_year =$request->production_year;
        $video->release_year =$request->release_year;
        $video->photo =$request->photo;
        $video->method_of =$request->method_of;
        $video->storyline =$request->storyline;
        $video->script = $request->script;
        $video->didyouknow =$request->didyouknow;

        if($video->save()){
            $video->keywords()->sync($request->keywords);
            $video->tags()->sync($request->tags);
            $video->cats()->sync($request->genres);
            return 1;
        }
    }

    public function destroy(Video $video){
        if($video->user_id == Auth::user()->id) {
            if (Video::destroy($video->id)) {
                Storage::disk('s3')->delete($video->aws_original_link);
                Storage::disk('s3out')->deleteDirectory($video->aws_prefix_folder);
                Storage::disk('s3thumbs')->deleteDirectory($video->aws_prefix_folder);

                return 1;
            }
        }
    }

    public function cleanup(Request $request){
        $prefix = explode('.',$request->prefix)[0];
        $this->removeFiles(storage_path('app/chunks'),$prefix."*.*");
    }

    function removeFiles($dir,$pattern)
    {
        $stack=array();
        array_push($stack,$dir);
        while(true){
            $dir=array_pop($stack);
            if($dir==NULL)
                break;
            $a= glob($dir.'/'.$pattern);
            print_r($a);
            foreach($a as $file) {
                if(is_dir($file)) {
                    array_push($stack,$file);
                    continue;
                }
                unlink($file);
            }
        }
    }

    public function fetchSubtitles(Video $video){
        return Subtitle::where('video_id','=',$video->id)->with('language')->get();
    }

}
