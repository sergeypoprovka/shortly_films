<?php

namespace App\Http\Controllers;

use App\Models\Video;
use App\Models\WatchLater;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WatchLaterController extends Controller
{
    public function index(){
        $watchlater = WatchLater::where(['user_id'=>Auth::user()->id])->whereHas('video', function($query){
            $query->where('approved','=','1');
        })->orderBy('created_at','DESC')->paginate(20);

        return view('user.watchlater', compact('watchlater'));
    }

    public function add(Request $request){
        $wl = WatchLater::firstOrCreate([
            'user_id'=>Auth::user()->id,
            'video_id'=>$request->video
        ]);
        return $wl;
    }

    public function remove(Request $request){
        $like = WatchLater::where([
            ['user_id','=',Auth::user()->id],
            ['video_id','=',$request->video]
        ])->first();

        return WatchLater::destroy($like->id);
    }
}
