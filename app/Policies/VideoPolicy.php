<?php

namespace App\Policies;

use App\Models\Video;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class VideoPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        if($user->role == "Filmmaker" || $user->role == "FullAccess"){
            return true;
        }
    }

    public function show(User $user, Video $video){
        if(auth()->user()->role == "Filmmaker" && $video->user_id == auth()->user()->id || auth()->user()->role == 'User' || auth()->user()->role == 'FullAccess'){
            return true;
        }
    }

    public function update(User $user, Video $video)
    {
        if(!$user->role == "Filmmaker" || !$user->role == "FullAccess"){
            abort('403','You\'re not authorized to use this action');
        }
    }

    public function delete(User $user, Video $video)
    {
        if(!$user->role == "Filmmaker" || !$user->role == "FullAccess"){
            abort('403','You\'re not authorized to use this action');
        }
    }

    public function upload(User $user)
    {
        if($user->role == "Filmmaker" || $user->role == "FullAccess"){
            return true;
        }
    }
}
